create table sis_par_usuario(
    id int auto_increment primary key,
    cedula varchar(20) unique,
    nombre varchar(200),
    login varchar(100),
    email varchar(200),
    password varchar(100),
    admin char(1) default 'N',
    activo char(1) default 'Y',
    alerta_creacion char(1) default 'N',
    alerta_edicion char(1) default 'N',
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_par_cliente(
    id int auto_increment primary key,
    id_nit varchar(30),
    nombres varchar(200),
    apellidos varchar(200),
    telefono varchar(100),
    direccion varchar(200),
    email varchar(100),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_par_controlador(
    id int auto_increment primary key,
    nombre varchar(200),
    nombre_clase varchar(200),
    validar_permiso char(1) default 'N',
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_par_permiso(
    id int auto_increment primary key,
    id_usuario int not null,
    id_controlador int not null,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key(id_usuario) references sis_par_usuario(id),
    foreign key(id_controlador) references sis_par_controlador(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_par_vehiculo_tipo(
    id int auto_increment primary key,
    nombre varchar(50) not null,
    mapa text,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_par_vehiculo(
    id int auto_increment primary key,
    id_propietario int not null,
    id_tipo int not null,
    nombre varchar(60),
    marca varchar(60),
    modelo varchar(60),
    motor varchar(60),
    serie varchar(60),
    color varchar(60),
    placa varchar(60),
    km varchar(60),
    cilindraje varchar(60),
    caja varchar(60),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key(id_propietario) references sis_par_cliente(id),
    foreign key(id_tipo) references sis_par_tipo_vehiculo(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_par_vehiculo_parte(
    id int auto_increment primary key,
    nombre varchar(60),
    llave varchar(60),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_par_vehiculo_tipo_parte(
    id int auto_increment primary key,
    id_tipo int,
    id_parte int,
    path text,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key(id_tipo) references sis_par_vehiculo_tipo(id),
    foreign key(id_parte) references sis_par_vehiculo_parte(id),
    UNIQUE INDEX id_tipo_unique (id_tipo, id_parte)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_cat_categoria(
    id int auto_increment primary key,
    nombre varchar(60),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_nom_cargo(
    id int auto_increment primary key,
    nombre varchar(60),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_nom_cargo_emplado(
    id int auto_increment primary key,
    id_cargo int,
    id_empleado int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_cargo) references sis_nom_cargo(id),
    foreign key (id_empleado) references sis_par_usuario(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_nom_prestamo(
    id int auto_increment primary key,
    id_empleado int,
    id_orden int,
    fecha timestamp,
    valor decimal(14,2) default 0,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_empleado) references sis_par_usuario(id),
    foreign key (id_orden) references sis_dg_documento(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_nom_periodo(
    id int auto_increment primary key,
    codigo varchar(20) unique,
    fecha_inicio date,
    fecha_fin date,
    fecha_liq timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    estado enum('N', 'C') default 'N',
    id_usuario_crea int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_usuario_crea) references sis_par_usuario(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_nom_periodo_documento(
    id int auto_increment primary key,
    id_documento int,
    id_periodo int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_documento) references sis_doc_documento(id),
    foreign key (id_periodo) references sis_nom_periodo(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_nom_periodo_empleado_detalle(
    id int auto_increment primary key,
    id_empleado int,
    id_documento int,
    id_periodo int,
    id_servicio int,
    id_cargo int,
    fecha date,
    valor decimal(14,2) default 0,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_empleado) references sis_par_usuario(id),
    foreign key (id_documento) references sis_doc_documento(id),
    foreign key (id_periodo) references sis_nom_periodo(id),
    foreign key (id_servicio) references sis_cat_producto(id),
    foreign key (id_cargo) references sis_nom_cargo(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_nom_periodo_empleado_resumen(
    id int auto_increment primary key,
    id_empleado int,
    id_periodo int,
    valor decimal(14,2) default 0,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_empleado) references sis_par_usuario(id),
    foreign key (id_periodo) references sis_nom_periodo(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_cat_producto(
    id int auto_increment primary key,
    nombre varchar(200),
    precio numeric(14,2) default 0,
    tipo enum ('P', 'S', 'H') default 'P',
    precio_abierto enum('Y', 'N') default 'N',
    existencias integer default 0,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_cat_servicio_cargo(
    id int auto_increment primary key,
    id_producto int,
    id_cargo int,
    porcentaje numeric(10,3) default 0,
    cantidad int default 1,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_producto) references sis_cat_producto(id),
    foreign key (id_cargo) references sis_nom_cargo(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_doc_tipo(
    id int auto_increment primary key,
    nombre varchar(200),
    sigla varchar(4),
    tipo enum ('E', 'S') default 'E',
    tipo_mov enum ('M', 'F', 'O', 'S', 'C', 'P', 'N') default 'M', /* Movimiento, Factura, Orden Entrada, Orden Servicio, Cotizacion, Peritaje, Nomina */
    url varchar(200),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

CREATE TABLE sis_doc_consecutivo (
    id INT(11) NOT NULL AUTO_INCREMENT primary key,
    id_tipo INT(11) NULL DEFAULT NULL unique,
    consecutivo INT(11) NULL DEFAULT '0',
    created_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at TIMESTAMP NOT NULL DEFAULT '0000-00-00 00:00:00',
    FOREIGN KEY (id_tipo) REFERENCES sis_doc_tipo (id)
)
COLLATE='utf8_general_ci'
ENGINE=InnoDB;

create table sis_fac_medio_pago(
    id int auto_increment primary key,
    nombre varchar(200),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_doc_documento(
    id int auto_increment primary key,
    id_tipo int,
    id_cliente int,
    id_vehiculo int,
    num int,
    fecha date,
    cantidad decimal(14,2) default 0,
    valor_descuento decimal(14,2) default 0,
    valor_iva decimal(14,2) default 0,
    valor_bruto decimal(14,2) default 0,
    valor_total decimal(14,2) default 0,
    observaciones text,
    id_usuario_crea int,
    id_doc_rel int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    UNIQUE INDEX `consecutivo_unique` (`id_tipo`, `num`),
    foreign key(id_tipo) references sis_doc_tipo(id),
    foreign key(id_cliente) references sis_par_cliente(id),
    foreign key(id_usuario_crea) references sis_par_usuario(id),
    foreign key(id_vehiculo) references sis_par_vehiculo(id),
    foreign key(id_doc_rel) references sis_doc_documento(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_mov_documento(
    id int auto_increment primary key,
    id_documento int,
    id_producto int,
    cantidad int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key(id_documento) references sis_doc_documento(id),
    foreign key(id_producto) references sis_cat_producto(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_fac_documento(
    id int auto_increment primary key,
    id_documento int,
    id_producto int,
    cantidad int,
    descuento decimal(14,2),
    iva decimal(14, 2),
    valor_unitario decimal(14,2),
    valor_descuento decimal(14,2),
    valor_iva decimal(14,2),
    valor_bruto decimal(14,2),
    valor_total decimal(14,2),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key(id_documento) references sis_doc_documento(id),
    foreign key(id_producto) references sis_cat_producto(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_fac_servicio(
    id int auto_increment primary key,
    id_documento int,
    id_docfac int,
    id_cargo int,
    id_empleado int,
    id_producto int,
    porcentaje numeric(10,3) default 0,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key(id_documento) references sis_doc_documento(id),
    foreign key(id_docfac) references sis_fac_documento(id),
    foreign key(id_cargo) references sis_nom_cargo(id),
    foreign key(id_empleado) references sis_par_usuario(id),
    foreign key(id_producto) references sis_cat_producto(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_fac_abono(
    id int auto_increment primary key,
    id_documento int,
    id_medio int,
    valor decimal(14,2),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key(id_documento) references sis_doc_documento(id),
    foreign key(id_medio) references sis_fac_medio_pago(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_dg_opcion(
    id int auto_increment primary key,
    nombre varchar(255),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_dg_categoria(
    id int auto_increment primary key,
    nombre varchar(255),
    requiere_cant  enum('Y', 'N') default 'N',
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_dg_item(
    id int auto_increment primary key,
    nombre varchar(255),
    id_categoria int,
    requiere_cant enum('Y', 'N') default 'N',
    unm varchar(10),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_cat) references sis_dg_categoria(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_dg_itemdoc(
    id int auto_increment primary key,
    id_tipo int,
    id_item int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_tipo) references sis_doc_tipo(id),
    foreign key (id_item) references sis_dg_item(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_dg_documento(
    id int auto_increment primary key,
    id_documento int,
    motor varchar(60),
    color varchar(60),
    km varchar(60),
    cilindraje varchar(60),
    caja varchar(60),
    observaciones text,
    estado_general text,
    nivel_combustible char(1),
    avaluo decimal(15, 0) default 0,
    dg_inicial text,
    recomendaciones text,
    estado enum('N','D','C'),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_doc) references sis_doc_documento(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_dg_doc_diagnostico(
    id int auto_increment primary key,
    id_doc_dg int,
    id_item_dg int,
    id_opc_dg int,
    cantidad int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_doc_dg) references sis_dg_documento(id),
    foreign key (id_item_dg) references sis_dg_item(id),
    foreign key (id_opc_dg) references sis_dg_opcion(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_dg_doc_producto(
    id int auto_increment primary key,
    id_doc_dg int,
    id_producto int,
    cantidad int,
    valor_unitario decimal(14,2),
    valor_total decimal(14,2),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_doc_dg) references sis_dg_documento(id),
    foreign key (id_producto) references sis_cat_producto(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_dg_doc_imagen(
    id int auto_increment primary key,
    id_doc_dg int,
    archivo varchar(300),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_doc_dg) references sis_dg_documento(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_dg_doc_parte(
    id int auto_increment primary key,
    id_doc_dg int,
    id_parte int,
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key (id_doc_dg) references sis_dg_documento(id),
    foreign key (id_parte) references sis_par_vehiculo_parte(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

create table sis_cot_documento(
    id int auto_increment primary key,
    id_documento int,
    id_producto int,
    cantidad int,
    descuento decimal(14,2),
    iva decimal(14, 2),
    valor_unitario decimal(14,2),
    valor_descuento decimal(14,2),
    valor_iva decimal(14,2),
    valor_bruto decimal(14,2),
    valor_total decimal(14,2),
    created_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    updated_at timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
    foreign key(id_documento) references sis_doc_documento(id),
    foreign key(id_producto) references sis_cat_producto(id)
)COLLATE='utf8_general_ci' ENGINE=InnoDB;

