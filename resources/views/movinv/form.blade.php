@extends('master')

@section('js_header')
<script>
    (function($){
        $(document).ready(function() {
            $("#btnAdicionarReg").click(function(e) {
                e.preventDefault();
                var $html = $("#registro").html();
                var $o = $("#registros").append($.parseHTML($html));
            });
            
            $("#btnGuardarDocumento").click(function(e) {
                e.preventDefault();                
                
                if (!confirm('¿Está seguro de terminar este documento?')){
                    return;
                }
                
                $("#loader").addClass("loading");
                
                $.ajax({
                    url: '{{ url("/movimientos/guardar") }}',
                    data: $("#form_movimiento").serialize(),
                    method: 'post'
                })
                .done(function(res) {
                    alert('El documento ha sido guardado exitosamente');
                    window.location.reload();
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    alert(jqXHR.responseJSON.error.message);
                })
                .always(function() {
                    $("#loader").removeClass("loading");
                });
            });
        });
    })(jQuery);
</script>
@stop

@section('content')
<h2>Realizar un movimiento de inventario</h2>
<form id="form_movimiento" name="form_movimiento" action="{{ url('movimientos/guardar') }}" method="post">
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">            
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Tipo</label>
        </div>
        <div class="medium-8 small-12 columns">
            <select id="id_tipo" name="id_tipo">
                @foreach($tipos as $t)
                <option value="{{ $t->id }}">{{ $t->sigla }} - {{ $t->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <fieldset>
        <legend>Productos</legend>
        <div class="row">
            <div class="medium-4 small-12 columns">
                <strong>Producto</strong>
            </div>
            <div class="medium-2 small-12 columns text-right">
                <strong>Cantidad</strong>
            </div>
            <hr />
        </div>
        <div id="registro">
            <div class="row">                
                <div class="medium-4 small-12 columns">
                    <select id="producto" name="id_producto[]">
                        @foreach($productos as $p)
                        <option value="{{ $p->id }}">{{ $p->nombre }}</option>
                        @endforeach
                    </select>                    
                </div>
                <div class="medium-2 small-12 columns end">
                    <input type="text" name="cantidad[]" id="cantidad" value="0" class="text-right" />
                </div>
            </div>
        </div>
        <div id="registros">

        </div>

        <div class="row">
            <div class="small-12 columns">
                <a class="small button rojo right" href="#" id="btnAdicionarReg" />A&ntilde;adir registro</a>
            </div>
        </div>
    </fieldset>
    <div class="row">
        <div class="small-12 columns text-right">
            <a class="button gris" href="{{ url('/movimientos/') }}" />Cancelar</a>                   
            <input id="btnGuardarDocumento" type="submit" value="Guardar" class="button naranja" />                    
        </div>
    </div>              
</form>
@stop