@extends('master')

@section('content')
<div class="row titulo lista">
    <div class="small-12 columns">Detalle de movimiento</div>
</div> 
<div class="row item lista">
    <div class="small-12 columns">
        <strong>Tipo de documento: </strong> {{ $documento->tipoDocumento->sigla }} - {{ $documento->tipoDocumento->nombre }}<br />
        <strong>N&uacute;mero de documento: </strong> {{ $documento->num }}<br />
        <strong>Fecha de creaci&oacute;n: </strong> {{ $documento->fecha }}<br />
        <strong>Creado por: </strong> {{ $documento->usuarioCreacion->nombre }}<br />

    </div>
</div>        
<div class="row item lista">
    <div class="small-6 columns">Producto</div>            
    <div class="small-6 columns">Cantidad</div>
</div>

@foreach($documento->movimientos as $m)
<div class="row item lista">            
    <div class="small-6 columns">{{ $m->producto->nombre }}</div>
    <div class="small-6 columns">{{ $m->cantidad }}</div>
</div>
@endforeach

<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('/movimientos/') }}" class="button">Regresar</a>
    </div>
</div>
@stop