@if (sizeof($vehiculo->documentos))
<div class="row titulo lista">
    <div class="small-12 columns">Documentos relacionados</div>
</div>
<div class="row item lista">
    <div class="small-2 columns">Documento</div>
    <div class="small-3 columns">Fecha</div>
    <div class="small-3 columns end">Ver documento</div>
</div>
@foreach($vehiculo->documentos as $d)
<div class="row item lista">
    <div class="small-2 columns">{{ $d->tipoDocumento->sigla }} - {{ $d->num }}</div>
    <div class="small-3 columns">{{ $d->fecha }}</div>
    <div class="small-3 columns end">
        <a href="{{ url($d->tipoDocumento->url)."/detalle/".$d->id }}" target="_blank"><i class="fi-magnifying-glass"></i></a>
        <a href="{{ url($d->tipoDocumento->url)."/pdf/".$d->id }}" target="_blank"><i class="fi-page-pdf"></i></a>
    </div>
</div>        
@endforeach

@else

<h1 class="text-center">Este veh&iacute;culo a&uacute;n no tiene documentos asociados</h1>

@endif
