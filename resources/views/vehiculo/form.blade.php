@extends('master')

@section('js_header')
<script>
    (function($, window){
        $(document).ready(function() {
            $("#btnBuscarCliente").click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: '{{ url("/clientes/buscar") }}',
                    method: 'post',
                    data: { 'cedula': $("#cedula_propietario").val(), _token: '{!! csrf_token() !!}' },
                    dataType: 'json',
                    success: function(json){
                        if (!_.isEmpty(json)){
                            $("#nombre_propietario").val(json.nombres);
                            $("#apellido_propietario").val(json.apellidos);
                        }
                        else{
                            alert('Cliente no encontrado');
                        }
                    }
                });
            });
        });
    })(jQuery, window)
</script>
@stop

@section('content')
<h2>Datos del veh&iacute;culo</h2>
<form id="form_cargo" name="form_cargo" action="{{ url('vehiculos/guardar') }}" method="post">
    <input type="hidden" id="id" name="id" value="{{ $vehiculo->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-2 small-12 columns">
            <label for="nombre">Nombre</label>
        </div>
        <div class="medium-4 small-12 columns end">
            <input type="text" name="nombre" id="nombre" value="{{ $vehiculo->nombre }}" />
        </div>
        <div class="medium-2 small-12 columns">
            <label for="placa">Placa</label>
        </div>
        <div class="medium-4 small-12 columns end">
            <input type="text" name="placa" id="placa" value="{{ $vehiculo->placa }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-2 small-12 columns">
            <label for="id_tipo">Tipo de veh&iacute;culo</label>
        </div>
        <div class="medium-4 small-12 columns end">
            <select id="id_tipo" name="id_tipo">
                @foreach($tipos as $t)
                <option value="{{ $t->id }}" @if($t->id == $vehiculo->id_tipo) selected="selected" @endif>{{ $t->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <fieldset>
        <legend>Propietario</legend>
        <div class="row">
            <div class="medium-2 small-12 columns">
                <label for="cedula_propietario">C&eacute;dula</label>
            </div>
            <div class="medium-4 small-12 columns end">
                <div class="row collapse">
                    <div class="small-9 columns"> 
                        <input type="hidden" name="id_propietario" id="id_propietario" value="{{ $vehiculo->id_propietario }}" />
                        <input type="text" name="cedula_propietario" id="cedula_propietario" value="@if(sizeof($vehiculo->propietario)){{ $vehiculo->propietario->id_nit }}@endif" />
                    </div>
                    <div class="small-3 columns"><button class="button default rojo tiny" id="btnBuscarCliente">Buscar</button></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="medium-2 small-12 columns">
                <label for="nombre_propietario">Nombre</label>
            </div>
            <div class="medium-4 small-12 columns">
                <input type="text" name="nombre_propietario" id="nombre_propietario" value="@if(sizeof($vehiculo->propietario)){{ $vehiculo->propietario->nombres }}@endif" />
            </div>
            <div class="medium-2 small-12 columns">
                <label for="apellido_propietario">Apellido</label>
            </div>
            <div class="medium-4 small-12 columns">
                <input type="text" name="apellido_propietario" id="apellido_propietario" value="@if(sizeof($vehiculo->propietario)){{ $vehiculo->propietario->apellidos }}@endif" />
            </div>
        </div>
    </fieldset>
    <div class="row">
        <div class="medium-2 small-12 columns">
            <label for="marca">Marca</label>
        </div>
        <div class="medium-4 small-12 columns">
            <input type="text" name="marca" id="marca" value="{{ $vehiculo->marca }}" />
        </div>
        <div class="medium-2 small-12 columns">
            <label for="modelo">Modelo</label>
        </div>
        <div class="medium-4 small-12 columns">
            <input type="text" name="modelo" id="modelo" value="{{ $vehiculo->modelo }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-2 small-12 columns">
            <label for="motor">Motor</label>
        </div>
        <div class="medium-4 small-12 columns">
            <input type="text" name="motor" id="motor" value="{{ $vehiculo->motor }}" />
        </div>
        <div class="medium-2 small-12 columns">
            <label for="serie">Serie</label>
        </div>
        <div class="medium-4 small-12 columns">
            <input type="text" name="serie" id="serie" value="{{ $vehiculo->serie }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-2 small-12 columns">
            <label for="color">Color</label>
        </div>
        <div class="medium-4 small-12 columns">
            <input type="text" name="color" id="color" value="{{ $vehiculo->color }}" />
        </div>
        <div class="medium-2 small-12 columns">
            <label for="km">KM</label>
        </div>
        <div class="medium-4 small-12 columns">
            <input type="text" name="km" id="km" value="{{ $vehiculo->km }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-2 small-12 columns">
            <label for="cilindraje">Cilindraje</label>
        </div>
        <div class="medium-4 small-12 columns">
            <input type="text" name="cilindraje" id="cilindraje" value="{{ $vehiculo->cilindraje }}" />
        </div>
        <div class="medium-2 small-12 columns">
            <label for="caja">Caja</label>
        </div>
        <div class="medium-4 small-12 columns">
            <input type="text" name="caja" id="caja" value="{{ $vehiculo->caja }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/vehiculos/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@include('vehiculo.documentos', array("vehiculo" => $vehiculo))
@stop