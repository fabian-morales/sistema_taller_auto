@extends('masterPdf')

@section('content')
{{--*/ $docDg = $documento->docDiagnostico->first()  /*--}}
<table cellspacing="5" cellpadding="0" class="sep">
    <tr>
        <td><strong>Documento</strong></td>
        <td>{{  $documento->tipoDocumento->nombre . ' ' . $documento->tipoDocumento->sigla }} - {{ $documento->num }}</td>
    </tr>
    <tr>
        <td><strong>Fecha:</strong></td>
        <td>{{ $documento->fecha }}</td>
    </tr>
    <tr>
        <td><strong>Cliente:</strong></td>
        <td>{{ $documento->cliente->id_nit }} - {{ $documento->cliente->nombres }}</td>
    </tr>
</table>

<h2>Datos del veh&iacute;culo</h2>
<div class='bordes sep'>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td><strong>Placa</strong></td>
            <td>{{ $documento->vehiculo->placa }}</td>
            <td><strong>Marca</strong></td>
            <td>{{ $documento->vehiculo->marca }}</td>
        </tr>
        <tr>
            <td><strong>Tipo</strong></td>
            <td colspan='3'>{{ $documento->vehiculo->tipo->nombre }}</td>
        </tr>
        <tr>
            <td><strong>Color</strong></td>
            <td>{{ $docDg->color }}</td>
            <td><strong>Motor</strong></td>
            <td>{{ $docDg->motor }}</td>
        </tr>
        <tr>
            <td><strong>Kilometraje</strong></td>
            <td>{{ $docDg->km }}</td>
            <td><strong>Cilindraje</strong></td>
            <td>{{ $docDg->cilindraje }}</td>
        </tr>
        <tr>
            <td><strong>Caja</strong></td>
            <td>{{ $docDg->caja }}</td>
            <td><strong>Nivel de combustible</strong></td>
            <td>{{ $niveles[$docDg->nivel_combustible] }}</td>
        </tr>
        <tr>
            <td><strong>Estado general</strong></td>
            <td colspan='3'>{{ $docDg->estado_general }}</td>
        </tr>
    </table>
</div>

@if(!empty($docDg->dg_inicial))
<h2>Diagn&oacute;stico inicial</h2>
<div class='bordes sep'>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td>{{ $docDg->dg_inicial }}</td>
        </tr>
    </table>
</div>
@endif

@if (!empty($documento->observaciones))
<h2>Observaciones</h2>
<div class='bordes sep'>
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td>{{ $documento->observaciones }}</td>
        </tr>
    </table>
</div>
@endif

<h2>Revisi&oacute;n de partes</h2>
<div class='bordes sep'>
    <table cellspacing="0" cellpadding="0">
        <tr class='doble'>
            <td style='width: 27%;'><strong>Parte</strong></td>
            <td style='width: 5%;' class="right"><strong>Cantidad</strong></td>
            @foreach ($opciones as $o)
            <td style='width: 7%;' class="center"><strong>{{ $o->nombre }}</strong></td>
            @endforeach
            <td style='width: 40%;'><strong>Observaciones</strong></td>
        </tr>

        @foreach($categorias as $c)
        <tr>
            <td colspan="10"><strong>{{ $c->nombre }}</strong></td>
        </tr>

        @foreach($c->items as $i)
        <tr>
            <td>{{ $i->nombre }}</td>
            <td class="right">@if(sizeof($i->diagnosticos) && $i->requiere_cant == 'Y') {{ $i->diagnosticos->first()->cantidad . ' ' . $i->unm }} @endif</td>
            @foreach ($opciones as $o)
            <td class="center">@if(sizeof($i->diagnosticos) && $i->diagnosticos->first()->id_opc_dg == $o->id) X @endif</td>
            @endforeach
            <td>@if(sizeof($i->diagnosticos)) {{ $i->diagnosticos->first()->observaciones }} @endif</td>
        </tr>
        @endforeach

        @endforeach
    </table>
</div>

@if (sizeof($docDg->imagenes))
<h2>Im&aacute;genes</h2>

@foreach($docDg->imagenes as $i)
<div class='imagen'>
    <img src='{{ public_path('storage/imagenes/'.$documento->tipoDocumento->sigla.'/'.$documento->id).'/'.$i->id.'.'.$i->archivo }}' />
</div>
@endforeach

@endif
<div style="clear: both"></div>
@if (sizeof($docDg->partes))
<h2>Zonas resaltadas del veh&iacute;culo</h2>

<ul>
    @foreach($docDg->partes as $p)
    <li>{{ $p->parteVehiculo->nombre }}</li>
    @endforeach
</ul>
@endif

<div style="width: 100%; height: 150px;"></div>

<div style="width: 25%; float: left; margin-right: 50px; line-height: 40px; font-weight: bold; text-align: center; border-top: 2px solid #000">
    Chequeado por
</div>

<div style="width: 25%; float: left; margin-right: 50px; line-height: 40px; font-weight: bold; text-align: center; border-top: 2px solid #000">
    Firma cliente
</div>

@stop