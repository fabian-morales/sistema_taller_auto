<form name="form_prestamo" id="form_prestamo" method="post" action="{{ url('/orden/servicio/prestamo/guardar') }}">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" name="id_orden" value="{{ $documento->docDiagnostico->first()->id }}" />
    <div class="row">
        <h2>Registrar un pr&eacute;stamo</h2>
        <div class="medium-3 columns"><label for="id_empleado">Empleado</label></div>
        <div class="medium-9 columns">
            <select name="id_empleado">
                <option></option>
                @foreach ($empleados as $empleado)
                <option value="{{ $empleado->id }}">{{ $empleado->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="medium-3 columns"><label for="valor">Valor</label></div>
        <div class="medium-9 columns"><input type="text" name="valor" /></div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <input type="submit" class="button rojo right" value="Guardar" />
        </div>
    </div>
</form>