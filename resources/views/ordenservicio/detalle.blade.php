@extends('master')

@section('content')
{{--*/ $docDg = $documento->docDiagnostico->first()  /*--}}

<div class="row titulo lista">
    <div class="small-12 columns">Detalle de la orden de servicio</div>
</div>
<div class="row item lista">
    <div class="small-12 columns">
        <strong>Tipo de documento: </strong> {{ $documento->tipoDocumento->sigla }} - {{ $documento->tipoDocumento->nombre }}<br />
        <strong>N&uacute;mero de documento: </strong> {{ $documento->num }}<br />
        <strong>Fecha de creaci&oacute;n: </strong> {{ $documento->fecha }}<br />
        <strong>Creado por: </strong> {{ $documento->usuarioCreacion->nombre }}<br />
    </div>
</div>

<fieldset>
    <legend>Datos del veh&iacute;culo</legend>
    <div class="row item lista ligera">
        <div class="small-6 medium-3 columns"><strong>Placa</strong></div>
        <div class="small-6 medium-3 columns">{{ $documento->vehiculo->placa }}</div>
        <div class="small-6 medium-3 columns"><strong>Marca</strong></div>
        <div class="small-6 medium-3 columns">{{ $documento->vehiculo->marca }}</div>
    </div>
    <div class="row item lista ligera">
        <div class="small-6 medium-3 columns"><strong>Tipo</strong></div>
        <div class="small-6 medium-9 columns">{{ $documento->vehiculo->tipo->nombre }}</div>
    </div>
    <div class="row item lista ligera">
        <div class="small-6 medium-3 columns"><strong>Color</strong></div>
        <div class="small-6 medium-3 columns">{{ $docDg->color }}</div>
        <div class="small-6 medium-3 columns"><strong>Motor</strong></div>
        <div class="small-6 medium-3 columns">{{ $docDg->motor || $documento->vehiculo->motor }}</div>
    </div>
    <div class="row item lista ligera">
        <div class="small-6 medium-3 columns"><strong>Kilometraje</strong></div>
        <div class="small-6 medium-3 columns">{{ $docDg->km }}</div>
        <div class="small-6 medium-3 columns"><strong>Cilindraje</strong></div>
        <div class="small-6 medium-3 columns">{{ $docDg->cilindraje }}</div>
    </div>
    <div class="row item lista ligera">
        <div class="small-6 medium-3 columns"><strong>Caja</strong></div>
        <div class="small-6 medium-3 columns">{{ $docDg->caja }}</div>
    </div>
    <div class="row">
        <div class="small-6 medium-3 columns"><strong>Estado general</strong></div>
        <div class="small-6 medium-9 columns">{{ $docDg->estado_general }}</div>
    </div>
</fieldset>

@foreach($categorias as $c)
<fieldset>
    <legend>{{ $c->nombre }}</legend>
    <div class="row">
        <div class="small-4 columns"><strong>Revisi&oacute;n de</strong></div>
        <div class="small-2 columns"><strong>Cnt</strong></div>
        <div class="small-4 columns text-center">
            <strong>Estado</strong>
            <div class="row">
                @foreach($opciones as $o)
                <div class="small-3 columns">{{ $o->nombre }}</div>
                @endforeach
            </div>
        </div>
        <div class="small-2 columns"><strong>Observaciones</strong></div>
    </div>
    <hr />
    @foreach($c->items as $i)
    <div class="row separador_gris">
        <div class="small-4 columns">
            {{ $i->nombre }}
        </div>
        <div class="small-2 columns">
            @if ($i->requiere_cant == "Y")
            <div class="row">
                <div class="small-8 columns">@if(sizeof($i->diagnosticos) && $i->requiere_cant == 'Y') {{ $i->diagnosticos->first()->cantidad }} @endif</div>
                <div class="small-4 columns"><span>{{ $i->unm }}</span></div>
            </div>
            @else
            &nbsp;
            @endif
        </div>
        <div class="small-4 columns text-center">
            <div class="row">
                @foreach($opciones as $o)
                <div class="small-3 columns"><input type="radio" id="resp_item_{{ $i->id }}_opc{{ $o->id }}" name="resp[{{ $i->id }}]" value="{{ $o->id }}" disabled @if(sizeof($i->diagnosticos) && $i->diagnosticos->first()->id_opc_dg == $o->id) checked @endif /></div>
                @endforeach
            </div>
        </div>
        <div class="small-2 columns">@if(sizeof($i->diagnosticos)) {{ $i->diagnosticos->first()->observaciones }} @endif</div>
    </div>
    @endforeach
</fieldset>
@endforeach

@if (sizeof($docDg->productos))
<fieldset>
    <legend>Productos incluidos</legend>
    <div class="row item lista">
        <div class="small-4 columns"><strong>Producto</strong></div>
        <div class="small-2 columns"><strong>Cant.</strong></div>
        <div class="small-3 columns"><strong>Vlr. unit.</strong></div>
        <div class="small-3 columns"><strong>Vlr. total</strong></div>
    </div>

    @foreach($docDg->productos as $p)
    <div class="row item lista">
        <div class="small-4 columns">
            {{ $p->producto->nombre }}
        </div>            
        <div class="small-2 columns">{{ $p->cantidad }}</div>
        <div class="small-3 columns">${{ $p->valor_unitario }}</div>        
        <div class="small-3 columns">${{ $p->valor_total }}</div>
    </div>
    @endforeach
</fieldset>
@endif

@if (!empty($documento->observaciones) || !empty($docDg->recomendaciones))
<fieldset>
    <legend>Otros</legend>
    @if (!empty($documento->observaciones))
    <div class="row">
        <div class="small-12 columns">
            <div class="small-12 columns"><strong>Observaciones</strong></div>
            <div class="small-12 columns">{{ $documento->observaciones }}</div>
        </div>
    </div>
    @endif
    
    @if (!empty($docDg->recomendaciones))
    <div class="row">
        <div class="small-12 columns">
            <div class="small-12 columns"><strong>Recomendaciones del taller</strong></div>
            <div class="small-12 columns">{{ $docDg->recomendaciones }}</div>
        </div>
    </div>
    @endif
</fieldset>
@endif

<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('/orden/servicio/pdf/'.$documento->id) }}" class="button" target='_blank'><i class="fi-page-pdf"></i>&nbsp;Ver en PDF</a>
        <a href="{{ url('/orden/servicio/') }}" class="button">Regresar</a>
    </div>
</div>
@stop