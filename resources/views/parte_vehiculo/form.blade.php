@extends('master')

@section('js_header')

@stop

@section('content')
<h2>Datos de la parte de veh&iacute;culo</h2>
<form id="form_parte" name="form_parte" action="{{ url('vehiculos/partes/guardar') }}" method="post">
    <input type="hidden" id="id" name="id" value="{{ $parte->id }}" />
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-2 small-12 columns">
            <label for="nombre">Nombre</label>
        </div>
        <div class="medium-4 small-12 columns end">
            <input type="text" name="nombre" id="nombre" value="{{ $parte->nombre }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-2 small-12 columns">
            <label for="nombre">Identificador</label>
        </div>
        <div class="medium-4 small-12 columns end">
            <input type="text" name="llave" id="llave" value="{{ $parte->llave }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/vehiculos/partes/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />
        </div>
    </div>
</form>
@stop