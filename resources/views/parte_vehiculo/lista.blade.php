<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('vehiculos/partes/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de partes de veh&iacute;culos</div>
</div>
<div class="row item lista">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-3 columns">Nombre</div>
    <div class="small-2 columns">Editar</div>
</div>
@foreach($partes as $p)
<div class="row item lista">
    <div class="small-2 columns">{{ $p->id }}</div>
    <div class="small-3 columns">{{ $p->nombre }}</div>
    <div class="small-2 columns"><a href="{{ url('/vehiculos/partes/editar/'.$p->id) }}"><i class="fi-pencil"></i></a></div>
</div>        
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $partes->render() !!}
    </div>
</div>