@extends('master')

@section('content')
<h2>Datos del &iacute;tem de diagn&oacute;stico</h2>
<form id="form_dg_item" name="form_dg_item" action="{{ url('/diagnostico/item/guardar') }}" method="post">
    <input type="hidden" id="id" name="id" value="{{ $item->id }}" />
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Nombre</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="nombre" id="nombre" value="{{ $item->nombre }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="id_categoria">Categor&iacute;as</label>
        </div>
        <div class="medium-8 small-12 columns">
            <select id="id_categoria" name="id_categoria">
                @foreach ($categorias as $c)
                <option value="{{ $c->id }}" @if($c->id == $item->id_categoria) selected @endif>{{ $c->nombre }}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            Admite cantidades
        </div>
        <div class="medium-8 small-12 columns">
            <label for='requiere_cant_si'>Si 
                <input type="radio" name="requiere_cant" id="requiere_cant_si" value="Y" @if ($item->requiere_cant == 'Y') checked @endif />
            </label>
            <label for='requiere_cant_si'>No 
                <input type="radio" name="requiere_cant" id="requiere_cant_no" value="N" @if ($item->requiere_cant == 'N') checked @endif />
            </label>
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="unm">Unidad de medida</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="unm" id="unm" value="{{ $item->unm }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/diagnostico/item/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />                    
        </div>
    </div>
</form>
@stop