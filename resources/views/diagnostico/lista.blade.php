@section('js_header')
<script>
    (function ($, window) {
        $(document).ready(function() {
            $("a[rel='items']").click(function(e) {
                e.preventDefault();
                var $this = $(this);
                var $dataItems = "#" + $(this).attr("data-id");
                $($dataItems).toggle('slow', function() {
                    if ($($dataItems).is(':visible')){
                        $this.find('i').removeClass('fi-plus').addClass('fi-minus');
                        $this.find('span').html('Ocultar items');
                    }
                    else{
                        $this.find('i').removeClass('fi-minus').addClass('fi-plus');
                        $this.find('span').html('Mostrar items');
                    }
                });
            });
        });
    })(jQuery, window);
</script>
@stop
<form name="form_asociacion" id="form_asociacion" method="post" action="{{ url('diagnostico/item/asociar') }}">
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="small-12 columns">
            <a href="{{ url('/diagnostico/categoria/crear') }}" class="button rojo">Nueva categoria <i class="fi-plus"></i></a>
            <a href="{{ url('/diagnostico/item/crear') }}" class="button rojo">Nuevo item <i class="fi-plus"></i></a>
            <input type="submit" class="button rojo" value="Actualizar asociaci&oacute;n" />
        </div>
    </div>
    <div class="row titulo lista">
        <div class="small-12 columns">Lista de categor&iacute;as e &iacute;tems de diagn&oacute;sticos</div>
    </div>
    @foreach($categorias as $c)
    <div class="row item lista">
        <div class="small-2 columns">{{ $c->id }}</div>
        <div class="small-5 columns">{{ $c->nombre }}</div>                        
        <div class="small-2 columns"><a href="{{ url('/diagnostico/categoria/editar/'.$c->id) }}"><i class="fi-pencil"></i></a></div>
        <div class="small-2 columns"><a rel="items" href="#" data-id="items_{{ $c->id }}"><i class="fi-plus"></i> <span>Mostrar items</span></a></div>
    </div>

    <fieldset class="hide" id="items_{{ $c->id }}">
        <legend>Items</legend>

        <div class="row item lista">
            <div class="small-2 columns">Id</div>
            <div class="small-4 columns">Nombre</div>
            <div class="small-1 columns">Editar</div>
            <div class="small-5 columns">
                <div class="row">
                @foreach ($documentos as $d)
                    <div class="small-4 columns">{{ $d->nombre }}</div>
                @endforeach
                </div>
            </div>
        </div>

        @foreach ($c->items as $i)
        <div class="row item lista">
            <div class="small-2 columns">{{ $i->id }}</div>
            <div class="small-4 columns">{{ $i->nombre }}</div>
            <div class="small-1 columns"><a href="{{ url('/diagnostico/item/editar/'.$i->id) }}"><i class="fi-pencil"></i></a></div>
            <div class="small-5 columns">
                <div class="row">
                @foreach ($documentos as $d)

                {{--*/ $tipo = $i->tipo($d->id) /*--}}
                <div class="small-4 columns">
                    <input type="checkbox" id="doc_{{ $d->id }}_item_{{ $i->id }}" name="doc[{{ $i->id }}][{{ $d->id }}]" value="{{ $d->id }}" @if (sizeof($tipo)) checked @endif />
                </div>
                @endforeach
                </div>
            </div>
        </div>
        @endforeach
    </fieldset>
    @endforeach

    <div class="row">
        <div class="small-12 columns">
            <input type="submit" class="button rojo" value="Actualizar asociaci&oacute;n" />
        </div>
    </div>
</form>
