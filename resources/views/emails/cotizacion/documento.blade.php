@extends('emails')

@section('content')
    <img src="{{ asset('img/titulo.png') }}" />
    <p>       
        Apreciado {{ $documento->cliente->nombres }}, <br />

        Hacemos env&iacute;o de la cotizaci&oacute;n que solicit&oacute;.
    </p>
@stop
