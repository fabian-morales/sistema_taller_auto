@extends('master')

@section('content')
<h2>Gesti&oacute;n de productos</h2>
<form id="form_producto" name="form_producto" action="{{ url('productos/guardar') }}" method="post">
    <input type="hidden" id="id" name="id" value="{{ $producto->id }}" />
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Nombre</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="nombre" id="nombre" value="{{ $producto->nombre }}" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="login">Precio</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="precio" id="precio" value="{{ $producto->precio }}" />
        </div>
    </div>            
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label>Tipo</label>
        </div>
        <div class="medium-8 small-12 columns columns">
            <input type="radio" name="tipo" id="tipo_prod" value="P" @if($producto->tipo == "P") checked @endif /><label for="tipo_prod">Producto</label>
            <input type="radio" name="tipo" id="tipo_serv" value="S" @if($producto->tipo == "S") checked @endif /><label for="tipo_serv">Servicio</label>
            <input type="radio" name="tipo" id="tipo_serv" value="H" @if($producto->tipo == "H") checked @endif /><label for="tipo_serv">Herramienta</label>
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label>Precio abierto</label>
        </div>
        <div class="medium-8 small-12 columns columns">
            <input type="radio" name="precio_abierto" id="precio_abierto_si" value="Y" @if($producto->precio_abierto == "Y") checked @endif /><label for="precio_abierto_si">Si</label>
            <input type="radio" name="precio_abierto" id="precio_abierto_no" value="N" @if($producto->precio_abierto == "N") checked @endif /><label for="precio_abierto_No">No</label>
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns text-right">
            <a class="button gris" href="{{ url('/productos/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button naranja" />                    
        </div>
    </div>              
</form>

@if ($producto->id > 0 && $producto->tipo == 'S')
<h3>Cargos asociados al servicio</h3>
<div class="row item lista">
    <div class="small-4 columns">Cargo</div>
    <div class="small-2 columns">#</div>
    <div class="small-2 columns">%</div>
    <div class="small-2 columns">Editar</div>
    <div class="small-2 columns">Quitar</div>
</div>        
@foreach($producto->encargados as $e)
<div class="row item lista">
    <div class="small-4 columns">{{ $e->cargo->nombre }}</div>
    <div class="small-2 columns">{{ $e->cantidad }}</div>
    <div class="small-2 columns">{{ $e->porcentaje }}</div>
    <div class="small-2 columns"><a href="{{ url('/productos/editarCargo/'.$e->id) }}" data-featherlight><i class="fi-pencil">&nbsp;</i></div>
    <div class="small-2 columns"><a href="{{ url('/productos/quitarCargo/'.$e->id) }}"><i class="fi-x"></i></a></div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('/productos/adicionarCargo/'.$producto->id) }}" class="button small naranja right" data-featherlight><i class="fi-plus"></i>&nbsp;A&ntilde;adir cargo</a>
    </div>
</div>
@endif
@stop