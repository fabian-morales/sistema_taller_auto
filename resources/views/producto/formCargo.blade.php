<form name="form_cargo_serv" method="post" action="{{ url('productos/guardarCargo') }}">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" name="id" value="{{ $encargado->id }}" />
    <input type="hidden" id="id_producto_ed" name="id_producto" value="{{ $producto->id }}" />
    <div class="row">
        <h2>Editar cargos para el servicio</h2>
        <div class="medium-4 small-12 columns">
            <label for="id_cargo">Cargo</label>
        </div>
        <div class="medium-8 small-12 columns">
            <select name="id_cargo" id="id_cargo">
            @foreach($cargos as $c)
            <option value="{{ $c->id }}" @if($c->id == $encargado->id) selected @endif>{{ $c->nombre }}</option>
            @endforeach
            </select>
        </div>
        <div class="medium-4 small-12 columns">
            <label for="cantidad">Cantidad</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="number" name="cantidad" id="cantidad" value="{{ $encargado->cantidad }}" />
        </div>
        <div class="medium-4 small-12 columns">
            <label for="nombre">Porcentaje pago</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="porcentaje" id="nombre" value="{{ $encargado->porcentaje }}" />
        </div>
        <div class="medium-4 small-12 columns">
            <input type="submit" value="Guardar" class="button right" />
        </div>
    </div>
</form>