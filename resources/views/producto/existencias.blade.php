@extends('master')

@section('content')
<div class="row titulo lista">
    <div class="small-12 columns">Existencias de productos</div>
</div>        
<div class="row item lista">
    <div class="small-3 columns">Consecutivo</div>
    <div class="small-5 columns">Nombre</div>
    <div class="small-4 columns">Cantidad</div>
</div>

@foreach($productos as $p)
<div class="row item lista">
    <div class="small-3 columns">{{ $p->id }}</div>
    <div class="small-5 columns">{{ $p->nombre }}</div>
    <div class="small-4 columns">{{ $p->existencias }}</div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $productos->render() !!}
    </div>
</div>
@stop