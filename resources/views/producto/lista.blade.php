<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('productos/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Productos</div>
</div>        
<div class="row item lista">
    <div class="small-3 columns">N&uacute;m</div>
    <div class="small-5 columns">Nombre</div>
    <div class="small-2 columns">Editar</div>
    <div class="small-2 columns">Borrar</div>
</div>

@foreach($productos as $p)
<div class="row item lista">
    <div class="small-3 columns">{{ $p->id }}</div>
    <div class="small-5 columns">{{ $p->nombre }}</div>
    <div class="small-2 columns"><a href="{{ url('/productos/editar/'.$p->id) }}"><i class="fi-pencil"></i></a></div>
    <div class="small-2 columns"><a href="{{ url('/productos/borrar/'.$p->id) }}"><i class="fi-x"></i></a></div>
</div>
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $productos->render() !!}
    </div>
</div>