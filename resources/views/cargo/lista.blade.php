<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('cargos/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de cargos</div>
</div>
<div class="row item lista">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-6 columns">Nombre</div>
    <div class="small-4 columns">Editar</div>
</div>
@foreach($cargos as $c)
<div class="row item lista">
    <div class="small-2 columns">{{ $c->id }}</div>
    <div class="small-6 columns">{{ $c->nombre }}</div>                        
    <div class="small-4 columns"><a href="{{ url('/cargos/editar/'.$c->id) }}"><i class="fi-pencil"></i></a></div>
</div>        
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $cargos->render() !!}
    </div>
</div>