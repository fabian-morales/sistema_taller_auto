@extends('master')

@section('content')
<h2>Datos del cargo</h2>
<form id="form_cargo" name="form_cargo" action="{{ url('cargos/guardar') }}" method="post">
    <input type="hidden" id="id" name="id" value="{{ $cargo->id }}" />
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Nombre</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="nombre" id="nombre" value="{{ $cargo->nombre }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/cargos/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />                    
        </div>
    </div>              
</form>
@stop