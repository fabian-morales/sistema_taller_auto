<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('cotizacion/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">&Uacute;ltimas cotizaciones</div>
</div>
<div class="row item lista">
    <div class="small-3 columns">N&uacute;m</div>
    <div class="small-4 columns">Tipo</div>
    <div class="small-3 columns">Fecha</div>
    <div class="small-2 columns">Opciones</div>
</div>

@forelse($cotizaciones as $c)
<div class="row item lista">
    <div class="small-3 columns">{{ $c->num }}</div>
    <div class="small-4 columns">{{ $c->tipoDocumento->sigla }}</div>
    <div class="small-3 columns">{{ $c->fecha }}</div>
    <div class="small-2 columns">
        <a href="{{ url('/cotizacion/detalle/'.$c->id) }}" title="Ver Detalle"><i class="fi-magnifying-glass">&nbsp;</i></a>
        <a href="{{ url('/cotizacion/pdf/'.$c->id) }}" target='_blank' title="Ver en PDF"><i class="fi-page-pdf"></i>&nbsp;</a>
        <a href="{{ url('/cotizacion/facturar/'.$c->id) }}" title="Generar cuenta de cobro"><i class="fi-shopping-bag"></i>&nbsp;</a>
        <a href="{{ url('/cotizacion/enviar/'.$c->id) }}" title="Enviar cotizaci&oacute;n al cliente"><i class="fi-mail"></i>&nbsp;</a>
    </div>
</div>
@empty
    <p class="text-center">No se encontr&oacute; documentos</p>
@endforelse

<div class="row">
    <div class="small-12 columns text-center">
        {!! $cotizaciones->render() !!}
    </div>
</div>