@extends('master')

@section('content')
<div class="row titulo lista">
    <div class="small-12 columns">Detalle del documento</div>
</div> 
<fieldset>
    <legend>Documento</legend>
    <div class="row">
        <div class="small-3 columns"><strong>Tipo de documento: </strong></div>
        <div class="small-3 columns">{{ $documento->tipoDocumento->sigla }} - {{ $documento->tipoDocumento->nombre }}</div>
        <div class="small-3 columns"><strong>N&uacute;mero de documento: </strong></div>
        <div class="small-3 columns">{{ $documento->num }}</div>
        <div class="clearfix"></div>
        <div class="small-3 columns"><strong>Cliente: </strong></div>
        <div class="small-9 columns">{{ $documento->cliente->id_nit }} - {{ $documento->cliente->nombres }}</div>
        <div class="clearfix"></div>
        <div class="small-3 columns"><strong>Fecha de creaci&oacute;n: </strong></div>
        <div class="small-3 columns">{{ $documento->fecha }}</div>
        <div class="small-3 columns"><strong>Creado por: </strong></div>
        <div class="small-3 columns">{{ $documento->usuarioCreacion->nombre }}</div>
        <div class="clearfix"></div>
        <div class="small-3 columns"><strong>Cantidad items: </strong></div>
        <div class="small-3 columns">{{ $documento->cantidad }}</div>
        <div class="small-3 columns"><strong>Valor descuentos: </strong></div>
        <div class="small-3 columns">${{ $documento->valor_descuento }}</div>
        <div class="clearfix"></div>
        <div class="small-3 columns"><strong>Valor total: </strong></div>
        <div class="small-3 columns end">${{ $documento->valor_total }}</div>
    </div>
</fieldset>

@if(sizeof($documento->vehiculo))
<fieldset>
    <legend>Datos del veh&iacute;culo</legend>
    <div class="row item lista ligera">
        <div class="small-6 medium-3 columns"><strong>Placa</strong></div>
        <div class="small-6 medium-3 columns">{{ $documento->vehiculo->placa }}</div>
        <div class="small-6 medium-3 columns"><strong>Marca</strong></div>
        <div class="small-6 medium-3 columns">{{ $documento->vehiculo->marca }}</div>
    </div>
    <div class="row item lista ligera">
        <div class="small-6 medium-3 columns"><strong>Tipo</strong></div>
        <div class="small-6 medium-9 columns">{{ $documento->vehiculo->tipo->nombre }}</div>
    </div>
    <div class="row item lista ligera">
        <div class="small-6 medium-3 columns"><strong>Color</strong></div>
        <div class="small-6 medium-3 columns">{{ $documento->vehiculo->color }}</div>
        <div class="small-6 medium-3 columns"><strong>Motor</strong></div>
        <div class="small-6 medium-3 columns">{{ $documento->vehiculo->motor }}</div>
    </div>
    <div class="row item lista ligera">
        <div class="small-6 medium-3 columns"><strong>Kilometraje</strong></div>
        <div class="small-6 medium-3 columns">{{ $documento->vehiculo->km }}</div>
        <div class="small-6 medium-3 columns"><strong>Cilindraje</strong></div>
        <div class="small-6 medium-3 columns">{{ $documento->vehiculo->cilindraje }}</div>
    </div>
    <div class="row item lista ligera">
        <div class="small-6 medium-3 columns"><strong>Caja</strong></div>
        <div class="small-6 medium-3 columns end">{{ $documento->vehiculo->caja }}</div>
    </div>
</fieldset>
@endif

<fieldset>
    <legend>Productos</legend>
    <div class="row item lista fn-small">
        <div class="small-4 columns"><strong>Producto</strong></div>
        <div class="small-1 columns"><strong>Cant.</strong></div>
        <div class="small-1 columns"><strong>Dcto.</strong></div>
        <div class="small-2 columns"><strong>Vlr. unit.</strong></div>
        <div class="small-2 columns"><strong>Vlr. dcto.</strong></div>            
        <div class="small-2 columns"><strong>Vlr. total</strong></div>
    </div>

    @foreach($documento->cotizacion as $c)
    <div class="row item lista fn-small">
        <div class="small-4 columns">{{ $c->producto->nombre }}</div>            
        <div class="small-1 columns">{{ $c->cantidad }}</div>
        <div class="small-1 columns">{{ $c->descuento }}%</div>
        <div class="small-2 columns">${{ $c->valor_unitario }}</div>
        <div class="small-2 columns">${{ $c->valor_descuento }}</div>            
        <div class="small-2 columns">${{ $c->valor_total }}</div>
    </div>
    @endforeach
</fieldset>

<fieldset>
    <legend>Otros</legend>
    <div class="row">
        <div class="small-12 columns">
            <div class="small-12 columns"><strong>Observaciones</strong></div>
            <div class="small-12 columns">{{ $documento->observaciones }}</div>
        </div>
    </div>
</fieldset>

<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('/cotizacion/pdf/'.$documento->id) }}" class="button" target='_blank'><i class="fi-page-pdf"></i>&nbsp;Ver en PDF</a>
        <a href="{{ url('/cotizacion/enviar/'.$documento->id) }}" class="button"><i class="fi-mail"></i>&nbsp;Enviar cotizaci&oacute;n al cliente</a>
        <a href="{{ url('/cotizacion/') }}" class="button">Regresar</a>
    </div>
</div>
@stop
