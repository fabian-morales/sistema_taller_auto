@extends('master')

@section('content')
<div class="row">
    <div class="small-12 columns">
        <ul class="small-block-grid-2 medium-block-grid-4 large-block-grid-6">
            <li>
                <div class="boton tablero">
                    <a href="{{ url('/productos/') }}">
                        <img src="{{ asset('img/icono_catalogo.png') }}" />
                        <span>Cat&aacute;logo</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="boton tablero">
                    <a href="{{ url('/movimientos/') }}">
                        <img src="{{ asset('img/icono_mov.png') }}" />
                        <span>Mov. inventarios</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="boton tablero">
                    <a href="{{ url('/facturas/crear') }}">
                        <img src="{{ asset('img/icono_fact.png') }}" />
                        <span>Cuentas de cobro</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="boton tablero">
                    <a href="{{ url('/cotizacion/') }}">
                        <img src="{{ asset('img/icono_cot.png') }}" />
                        <span>Cotizaciones</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="boton tablero">
                    <a href="{{ url('/orden/entrada/') }}">
                        <img src="{{ asset('img/icono_entrada.png') }}" />
                        <span>Ordenes de entrada</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="boton tablero">
                    <a href="{{ url('/orden/servicio/') }}">
                        <img src="{{ asset('img/icono_servicio.png') }}" />
                        <span>Ordenes de servicio</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="boton tablero">
                    <a href="{{ url('/usuarios/') }}">
                        <img src="{{ asset('img/icono_empleados.png') }}" />
                        <span>Empleados</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="boton tablero">
                    <a href="{{ url('/vehiculos/') }}">
                        <img src="{{ asset('img/icono_carro.png') }}" />
                        <span>Veh&iacute;culos</span>
                    </a>
                </div>
            </li>
            <li>
                <div class="boton tablero">
                    <a href="{{ url('/nomina/periodo/') }}">
                        <img src="{{ asset('img/icono_nomina.png') }}" />
                        <span>N&oacute;mina</span>
                    </a>
                </div>
            </li>
        </ul>
    </div>            
</div>
@stop