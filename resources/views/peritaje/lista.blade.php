<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('orden/peritaje/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">&Uacute;ltimos peritajes</div>
</div>
<div class="row item lista">
    <div class="small-3 columns">N&uacute;m</div>
    <div class="small-4 columns">Tipo</div>
    <div class="small-3 columns">Fecha</div>
    <div class="small-2 columns">Ver</div>
</div>

@forelse($ordenes as $o)
<div class="row item lista">
    <div class="small-3 columns">{{ $o->num}}</div>
    <div class="small-4 columns">{{ $o->tipoDocumento->sigla }}</div>
    <div class="small-3 columns">{{ $o->fecha }}</div>
    <div class="small-2 columns">
        <a href="{{ url('/orden/peritaje/detalle/'.$o->id) }}"><i class="fi-magnifying-glass">&nbsp;</i></a>
        <a href="{{ url('/orden/peritaje/pdf/'.$o->id) }}" target='_blank' title="Ver en PDF"><i class="fi-page-pdf"></i>&nbsp;</a>

    </div>
</div>
@empty
    <p class="text-center">No se encontr&oacute; documentos</p>
@endforelse

<div class="row">
    <div class="small-12 columns text-center">
        {!! $ordenes->render() !!}
    </div>
</div>