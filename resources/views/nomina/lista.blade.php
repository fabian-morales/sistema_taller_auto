<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('nomina/periodo/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">&Uacute;ltimos periodos</div>
</div>        
<div class="row item lista">
    <div class="small-3 columns">N&uacute;m</div>
    <div class="small-4 columns">Codigo</div>
    <div class="small-3 columns">Fecha</div>            
    <div class="small-2 columns">Opciones</div>            
</div>

@forelse($periodos as $p)
<div class="row item lista">
    <div class="small-3 columns">{{ $p->id }}</div>
    <div class="small-4 columns">{{ $p->codigo }}</div>
    <div class="small-3 columns">{{ $p->fecha_inicio }}</div>            
    <div class="small-2 columns">
        <a href="{{ url('/nomina/periodo/detalle/'.$p->id) }}" title="Ver Detalle"><i class="fi-magnifying-glass">&nbsp;</i></a>
    </div>            
</div>
@empty
    <p class="text-center">No se encontr&oacute; periodos</p>
@endforelse

<div class="row">
    <div class="small-12 columns text-center">
        {!! $periodos->render() !!}
    </div>
</div>