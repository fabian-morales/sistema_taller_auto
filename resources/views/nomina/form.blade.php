@extends('master')

@section('js_header')
<script>
(function($, window){
    $(document).ready(function() {
        var $datePickerOpc = {
            dateFormat: "yy-mm-dd",
            numberOfMonths: 1,
            dayNames: [ "Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado" ],
            dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ],
            dayNamesShort: [ "Dom", "Lun", "Mar", "Mié", "Jue", "Vie", "Sáb" ],
            monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
            monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "Mau", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            currentText: "Hoy",
            closeText: "Aceptar",
            maxPicks: 1,
            showButtonPanel: true
        };
        
        $("#fecha_fin").datepicker($datePickerOpc);
        
        $("#btnLiquidar").click(function(e) {
            e.preventDefault();
            if ($("#fecha_fin").val() === '') {
                alert("Debe seleccionar la fecha de fin");
                return;
            }

            if (!confirm('¿Está seguro de realizar esta liquidación?')) {
                return;
            }

            $("#loader").addClass("loading");

            $.ajax({
                url: '{{ url("/nomina/periodo/liquidar") }}',
                data: $("#form_periodo").serialize(),
                method: 'post'
            })
            .done(function (res) {
                alert('Se ha realizado la liquidación exitosamente');
                window.location.reload();
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseJSON.error.message);
            })
            .always(function () {
                $("#loader").removeClass("loading");
            });
        });
    });
})(jQuery, window);
</script>
@stop

@section('content')
<h2>Liquidar periodo</h2>
<form id="form_periodo" name="form_periodo" action="{{ url('nomina/periodo/liquidar') }}" method="post">
    <input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Fecha inicio</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="fecha_inicio" id="fecha_inicio" value="{{ $fechaInicio }}" readonly="readonly" />
        </div>
    </div>
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Fecha Fin</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="fecha_fin" id="fecha_fin" value="" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/nomina/periodo/') }}" />Cancelar</a>
            <input id="btnLiquidar" type="button" value="Liquidar" class="button default naranja" />                    
        </div>
    </div>              
</form>
@stop