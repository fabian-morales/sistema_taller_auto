@extends('master')

@section('content')
<h2>Datos del medio de pago</h2>
<form id="form_medios" name="form_medio" action="{{ url('mediospago/guardar') }}" method="post">
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <input type="hidden" id="id" name="id" value="{{ $medio->id }}" />
    <div class="row">
        <div class="medium-4 small-12 columns">
            <label for="nombre">Nombre</label>
        </div>
        <div class="medium-8 small-12 columns">
            <input type="text" name="nombre" id="nombre" value="{{ $medio->nombre }}" />
        </div>
    </div>
    <div class="row">
        <div class="small-12 columns">
            <a class="button gris" href="{{ url('/mediospago/') }}" />Cancelar</a>
            <input type="submit" value="Guardar" class="button default" />                    
        </div>
    </div>              
</form>
@stop