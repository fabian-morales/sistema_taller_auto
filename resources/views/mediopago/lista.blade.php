<div class="row">
    <div class="small-12 columns">
        <a href="{{ url('mediospago/crear') }}" class="button rojo">Nuevo <i class="fi-plus"></i></a>
    </div>
</div>
<div class="row titulo lista">
    <div class="small-12 columns">Lista de medios de pago</div>
</div>
<div class="row item lista">
    <div class="small-2 columns">N&uacute;m</div>
    <div class="small-6 columns">Nombre</div>
    <div class="small-4 columns">Editar</div>
</div>
@foreach($medios as $m)
<div class="row item lista">
    <div class="small-2 columns">{{ $m->id }}</div>
    <div class="small-6 columns">{{ $m->nombre }}</div>                        
    <div class="small-4 columns"><a href="{{ url('/mediospago/editar/'.$m->id) }}"><i class="fi-pencil"></i></a></div>
</div>        
@endforeach
<div class="row">
    <div class="small-12 columns text-center">
        {!! $medios->render() !!}
    </div>
</div>