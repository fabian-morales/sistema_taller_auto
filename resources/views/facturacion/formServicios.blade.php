<form id="form_servicios" name="form_servicios" action="{{ url('facturas/servicio/guardar') }}" method="post">
    <input type="hidden" name="id_producto" id="id_producto" value="{{ $producto->id }}" />
	<input type="hidden" name="_token" value="{!! csrf_token() !!}">
    <h3>Seleccione las personas que prestar&aacute;n este servicio</h3>

    @if (sizeof($producto->encargados))
    
    @foreach($producto->encargados as $e)
    <fieldset>
        <legend>{{ $e->cargo->nombre }}</legend>
        <div class="row">
            <input type="hidden" name="id_cargo[{{ $e->cargo->id }}]" value="{{ $e->cargo->id }}" />
            @if (sizeof($e->cargo->empleados))
            <div class="small-12 columns">
                <select name="id_empleado[{{ $e->cargo->id }}]">
                @foreach ($e->cargo->empleados as $m)
                <option value="{{ $m->id }}" @if(sizeof($servicios) && sizeof($servicios["cargo_".$e->cargo->id]) && $m->id == $servicios["cargo_".$e->cargo->id]->id_empleado) selected @endif>{{ $m->nombre }}</option>
                @endforeach
                </select>
                @endif
            </div>
        </div>
    </fieldset>
    @endforeach
    
    @endif
    
    <div class="row">
        <div class="small-12 columns text-right">
            <a class="button gris" href="#" id="btnCancelar">Cancelar</a>                   
            <input type="button" id="btnGuardarServicio" value="Guardar" class="button naranja" />                    
        </div>
    </div>              
</form>