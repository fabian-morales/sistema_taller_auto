<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Prestamo extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nom_prestamo';
    
    public function empleado(){
        return $this->hasOne('App\User', 'id', 'id_empleado');
    }
}