<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PeriodoDetalle extends Model {    
    protected $table = 'nom_periodo_empleado_detalle';
    
    public function servicio(){
        return $this->hasOne('App\Models\Producto', 'id', 'id_servicio');
    }
    
    public function cargo(){
        return $this->hasOne('App\Models\Cargo', 'id', 'id_cargo');
    }
    
    public function documento(){
        return $this->hasOne('App\Models\Documento', 'id', 'id_documento');
    }
    
    public function empleado(){
        return $this->hasOne('App\User', 'id', 'id_empleado');
    }
}
