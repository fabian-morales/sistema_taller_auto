<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AbonoFac extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fac_abono';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('created_at', 'updated_at');
    protected $fillable = array('id', 'id_medio', 'id_documento', 'valor');
    
    public function medioPago(){
        return $this->belongsTo('App\Models\MedioPago', 'id_medio', 'id');
    }
    
    public function documento(){
        return $this->belongsTo('App\Models\Documento', 'id_documento', 'id');
    }
}