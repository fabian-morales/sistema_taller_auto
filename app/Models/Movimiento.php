<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Movimiento extends Model {    
    protected $table = 'mov_documento';
    
    public function producto(){
        return $this->hasOne('App\Models\Producto', 'id', 'id_producto');
    }
}

