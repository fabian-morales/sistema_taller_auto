<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class TipoVehiculo extends Model {    
    protected $table = 'par_vehiculo_tipo';
    protected $fillable = array('id', 'nombre');
    
    public function partes(){
        return $this->belongsToMany("App\Models\ParteVehiculo", "par_vehiculo_tipo_parte", "id_tipo", "id_parte")->withPivot('path');
    }
}
