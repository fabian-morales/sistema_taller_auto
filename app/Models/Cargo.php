<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Cargo extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'nom_cargo';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('created_at', 'updated_at');
    protected $fillable = array('id', 'nombre');
    
    public function servicios(){
        return $this->hasMany('App\Models\ServicioCargo', 'id_cargo');
    }
    
    function empleados(){
        return $this->belongsToMany('App\User', 'nom_cargo_empleado', 'id_cargo', 'id_empleado');
    }
    
    /*public static function boot()
    {
        parent::boot();
        
        static::created(function($producto)
        {
            $log = new DataLog();
            $log->tabla = "Producto";
            $log->objeto = $producto->toJson();
            $log->id_usuario = Auth::user()->id;
            $log->save();
        });

        static::updated(function($producto)
        {
            $log = new DataLog();
            $log->tabla = "Producto";
            $log->objeto = $producto->toJson();
            $log->id_usuario = Auth::user()->id;
            $log->save();
        });
    } */ 

}