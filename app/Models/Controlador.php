<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Controlador extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'par_controlador';
    protected $fillable = array('id', 'nombre', 'nombre_clase', 'validar_permiso');
    
    public function usuarios(){
        return $this->belongsToMany("App\User", "par_permiso", "id_controlador", "id_usuario");
    }
}
