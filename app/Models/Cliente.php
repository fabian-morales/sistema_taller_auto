<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model {


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'par_cliente';

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('created_at', 'updated_at');
    protected $fillable = array('id', 'id_nit', 'nombres', 'apellidos', 'telefono', 'direccion', 'email');
    
    public function vehiculos(){
        return $this->hasMany('App\Models\Vehiculo', 'id_propietario');
    }
    
    /*public static function boot()
    {
        parent::boot();
        
        static::created(function($producto)
        {
            $log = new DataLog();
            $log->tabla = "Producto";
            $log->objeto = $producto->toJson();
            $log->id_usuario = Auth::user()->id;
            $log->save();
        });

        static::updated(function($producto)
        {
            $log = new DataLog();
            $log->tabla = "Producto";
            $log->objeto = $producto->toJson();
            $log->id_usuario = Auth::user()->id;
            $log->save();
        });
    } */ 

}