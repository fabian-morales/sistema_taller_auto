<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class PeriodoResumen extends Model {    
    protected $table = 'nom_periodo_empleado_resumen';
    
    public function empleado(){
        return $this->hasOne('App\User', 'id', 'id_empleado');
    }
}
