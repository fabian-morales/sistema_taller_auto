<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class ImagenDg extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dg_doc_imagen';
    
    public function documento(){
        return $this->belongsTo("App\Models\DocumentoDg", "id_doc_dg");
    }

}
