<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Vehiculo extends Model {    
    protected $table = 'par_vehiculo';
    protected $fillable = array('id', 'nombre', 'id_propietario', 'id_tipo', 'marca', 'modelo', 'motor', 'serie', 'color', 'placa', 'km', 'cilindraje', 'caja');
    
    public function propietario(){
        return $this->hasOne('App\Models\Cliente', 'id', 'id_propietario');
    }
    
    public function tipo(){
        return $this->hasOne('App\Models\TipoVehiculo', 'id', 'id_tipo');
    }

    public function documentos(){
        return $this->hasMany('App\Models\Documento', 'id_vehiculo');
    }
}

