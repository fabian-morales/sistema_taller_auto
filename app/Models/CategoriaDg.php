<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class CategoriaDg extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dg_categoria';
    protected $fillable = array('id', 'nombre');
    
    public function items(){
        return $this->hasMany("App\Models\ItemDg", "id_categoria");
    }
}
