<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Diagnostico extends Model {
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'dg_doc_diagnostico';
    
    public function documento(){
        return $this->belongsTo("App\Models\DocumentoDg", "id_doc_dg");
    }
    
    public function item(){
        return $this->belongsTo("App\Models\ItemDg", "id_item_dg");
    }
    
    public function opcion(){
        return $this->belongsTo("App\Models\OpcionDg", "id_opcion_dg");
    }

}
