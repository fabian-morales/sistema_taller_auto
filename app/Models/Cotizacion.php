<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Cotizacion extends Model {    
    protected $table = 'cot_documento';
    
    public function producto(){
        return $this->hasOne('App\Models\Producto', 'id', 'id_producto');
    }
}
