<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Periodo extends Model {    
    protected $table = 'nom_periodo';
    
    /*public function producto(){
        return $this->hasOne('App\Models\Producto', 'id', 'id_producto');
    }*/
    
    function documentos(){
        return $this->belongsToMany('App\Models\Documento', 'nom_periodo_documento', 'id_periodo', 'id_documento');
    }
    
    function detalles(){
        return $this->hasMany("App\Models\PeriodoDetalle", "id_periodo");
    }
    
    function resumenes(){
        return $this->hasMany("App\Models\PeriodoResumen", "id_periodo");
    }
}

