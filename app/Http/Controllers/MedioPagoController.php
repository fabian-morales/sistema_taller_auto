<?php

namespace App\Http\Controllers;

use View;
use Input;
use Redirect;
use App\Http\Controllers\AdminController;
use App\Models\MedioPago;

class MedioPagoController extends AdminController {

    public function mostrarIndex(){       
        $medios = MedioPago::paginate(20);
        return View::make('mediopago.index', array("medios" => $medios));
    }
    
    public function mostrarFormMedio($medio){       
        if (!sizeof($medio)){
            $medio = new MedioPago();
        }
        
        return View::make("mediopago.form", array("medio" => $medio));
    }
    
    public function crearMedio(){       
        return $this->mostrarFormMedio(new MedioPago());
    }
    
    public function editarMedio($id){        
        $medio = MedioPago::find($id);
        if (!sizeof($medio)){
            return Redirect::action('MedioPagoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el medio de pago");
        }
        
        return $this->mostrarFormMedio($medio);
    }
    
    public function guardarMedio(){        
        $id = Input::get("id");
                
        $medio = MedioPago::find($id);
        if (!sizeof($medio)){
            $medio = new MedioPago();
        }
                
        $medio->fill(Input::all());                
        
        if ($medio->save()){
            return Redirect::action('MedioPagoController@mostrarIndex')->with("mensaje", "Medio de pago guardado exitosamente");
        }
        else{
            return Redirect::action('MedioPagoController@mostrarIndex')->with("mensajeError", "No se pudo guardar el medio de pago");
        }
    }    
}