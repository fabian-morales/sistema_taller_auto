<?php

namespace App\Http\Controllers;

use View;
use Input;
use Redirect;
use App\Http\Controllers\AdminController;
use App\Models\Cargo;

class CargoController extends AdminController {

    public function mostrarIndex(){       
        $cargos = Cargo::paginate(20);
        return View::make('cargo.index', array("cargos" => $cargos));
    }
    
    public function mostrarFormCargo($cargo){       
        if (!sizeof($cargo)){
            $cargo = new Cargo();
        }
        
        return View::make("cargo.form", array("cargo" => $cargo));
    }
    
    public function crearCargo(){       
        return $this->mostrarFormCargo(new Cargo());
    }
    
    public function editarCargo($id){        
        $cargo = Cargo::find($id);
        if (!sizeof($cargo)){
            return Redirect::action('CargoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el cargo");
        }
        
        return $this->mostrarFormCargo($cargo);
    }
    
    public function guardarCargo(){        
        $id = Input::get("id");
                
        $cargo = Cargo::find($id);
        if (!sizeof($cargo)){
            $cargo = new Cargo();
        }
                
        $cargo->fill(Input::all());                
        
        if ($cargo->save()){
            return Redirect::action('CargoController@mostrarIndex')->with("mensaje", "Cargo guardado exitosamente");
        }
        else{
            return Redirect::action('CargoController@mostrarIndex')->with("mensajeError", "No se pudo guardar el cargo");
        }
    }    
}