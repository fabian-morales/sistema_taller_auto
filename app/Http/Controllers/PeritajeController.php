<?php

namespace App\Http\Controllers;

use View;
use Input;
use Auth;
use Redirect;
use DB;
use Exception;
use App\Http\Controllers\AdminController;

class PeritajeController extends AdminController {

    public function mostrarIndex() {
        $ordenes = \App\Models\Documento::with('tipoDocumento')->whereHas('tipoDocumento', function($query) {
                    $query->where('tipo_mov', 'P');
                })->orderBy('created_at', 'desc')->paginate(20);

        return View::make('peritaje.index', array("ordenes" => $ordenes));
    }
    
    public function obtenerListaDocs(){
        $fechaInicio = !empty(Input::get("fecha_inicio")) ? Input::get("fecha_inicio") : '2000-01-01';
        $fechaFin = !empty(Input::get("fecha_fin")) ? Input::get("fecha_fin") : '2100-12-31';
        $cedula = Input::get("cedula");
        $placa = Input::get("placa");
        
        $ordenes = \App\Models\Documento::with('tipoDocumento')->whereHas('tipoDocumento', function($query) {
            $query->where('tipo_mov', 'P');
        })->where('fecha', '>=', $fechaInicio)->where('fecha', '<=', $fechaFin);
        
        if (!empty($cedula)){
            $ordenes = $ordenes->whereHas("cliente", function($q) use ($cedula) {
                $q->where("id_nit", $cedula);
            });
        }
        
        if (!empty($placa)){
            $ordenes = $ordenes->whereHas("vehiculo", function($q) use ($placa) {
                $q->where("placa", $placa);
            });
        }
        
        $ordenes = $ordenes->orderBy('created_at', 'desc')->paginate(20);
        
        return View::make('peritaje.lista', array("ordenes" => $ordenes));
    }

    public function verDetalle($id) {
        $documento = \App\Models\Documento::with(array("docDiagnostico.imagenes", "docDiagnostico.partes.parteVehiculo", "usuarioCreacion", "cliente", "tipoDocumento", "vehiculo.tipo"))->where("id", $id)->first();

        if (!sizeof($documento)) {
            return Redirect::action('PeritajeController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        } else {
            $opciones = \App\Models\OpcionDg::all();

            $categorias = \App\Models\CategoriaDg::with(array(
                        "items" => function ($q) use ($documento) {
                            $q->whereHas("tipos", function($q) use ($documento) {
                                        $q->where("id_tipo", $documento->id_tipo);
                                    });
                        },
                        "items.tipos" => function($q) use ($documento) {
                            $q->where("id_tipo", $documento->id_tipo);
                        },
                        "items.diagnosticos" => function($q) use ($documento) {
                            $q->where("id_doc_dg", $documento->docDiagnostico->first()->id);
                        }
                    ))->whereHas("items.tipos", function($q) use ($documento) {
                        $q->where("id_tipo", $documento->id_tipo);
                    }
                    )->get();

            return View::make("peritaje.detalle", array("documento" => $documento, "categorias" => $categorias, "opciones" => $opciones));
        }
    }

    public function mostrarFormPeritaje($documento) {
        if (!sizeof($documento)) {
            $documento = new \App\Models\Documento();
        }

        $tipoDoc = \App\Models\TipoDocumento::where("tipo_mov", "P")->first();

        $categorias = \App\Models\CategoriaDg::with(array("items" => function ($q) use ($tipoDoc) {
                        $q->whereHas("tipos", function($q) use ($tipoDoc) {
                                    $q->where("id_tipo", $tipoDoc->id);
                                });
                    },
                    "items.tipos" => function($q) use ($tipoDoc) {
                        $q->where("id_tipo", $tipoDoc->id);
                    }))->whereHas("items.tipos", function($q) use ($tipoDoc) {
                    $q->where("id_tipo", $tipoDoc->id);
                })->get();

        $opciones = \App\Models\OpcionDg::all();
        $tipos = \App\Models\TipoDocumento::where("tipo_mov", "P")->get();
        $tiposVeh = \App\Models\TipoVehiculo::all();

        return View::make("peritaje.form", array("documento" => $documento, "categorias" => $categorias, "opciones" => $opciones, "tipos" => $tipos, "tipos_veh" => $tiposVeh));
    }

    public function crearPeritaje() {
        return $this->mostrarFormPeritaje(new \App\Models\Documento());
    }

    public function guardarPeritaje() {
        try {
            DB::beginTransaction();

            $documento = new \App\Models\Documento();
            $idTipoDoc = Input::get("id_tipo");
            $tipoDoc = \App\Models\TipoDocumento::find($idTipoDoc);

            $cedula = Input::get("cedula");
            $nombre = Input::get("nombre");
            $apellidos = Input::get("apellidos");
            $telefono = Input::get("telefono");
            $direccion = Input::get("direccion");

            $cliente = \App\Models\Cliente::where("id_nit", $cedula)->first();
            if (!sizeof($cliente)) {
                $cliente = new \App\Models\Cliente();
                $cliente->id_nit = $cedula;
            }

            $cliente->nombres = $nombre;
            $cliente->apellidos = $apellidos;
            $cliente->telefono = $telefono;
            $cliente->direccion = $direccion;

            if (!$cliente->save()) {
                throw new Exception('No se pudo guardar los datos del cliente');
            }

            $idVehiculo = Input::get("id_vehiculo");
            $vehiculo = \App\Models\Vehiculo::find($idVehiculo);
            
            if (!sizeof($vehiculo) || empty($idVehiculo)){
                //throw new Exception('El veh&iacute;culo ingresado no existe');
                $vehiculo = new \App\Models\Vehiculo();
                $vehiculo->id_propietario = $cliente->id;
            }
            
            $vehiculo->id_tipo = Input::get("tipo_vehiculo");    
            if (empty($vehiculo->id_tipo)){
                throw new Exception('Debe seleccionar el tipo de vehículo');
            }
            
            $vehiculo->marca = Input::get("marca");
            if (empty($vehiculo->marca)){
                throw new Exception('Debe seleccionar la marca del vehículo');
            }
            
            $vehiculo->motor = Input::get("motor");
            $vehiculo->serie = Input::get("serie");
            $vehiculo->color = Input::get("color");
            $vehiculo->modelo = Input::get("modelo");
            $vehiculo->placa = Input::get("placa");
            if (empty($vehiculo->placa)){
                throw new Exception('Debe seleccionar la placa del vehículo');
            }
            
            $vehiculo->km = Input::get("km");
            $vehiculo->cilindraje = Input::get("cilindraje");
            $vehiculo->caja = Input::get("caja");
            $vehiculo->nombre = $vehiculo->marca." - ".$vehiculo->placa;
            
            if (!$vehiculo->save()){
                throw new Exception('No se pudo crear el vehículo');
            }
            
            if ($vehiculo->id_propietario !== $cliente->id){
                throw new Exception('El vehículo ingresado no pertenece al cliente');
            }

            $documento->id_tipo = $idTipoDoc;
            $documento->fecha = date('Y-m-d H:i:s');
            $documento->id_cliente = $cliente->id;
            $documento->observaciones = Input::get("observaciones");
            $documento->id_usuario_crea = Auth::user()->id;
            $documento->id_vehiculo = $vehiculo->id;

            if (!$documento->save()) {
                throw new Exception('No se pudo guardar el documento');
            }

            $docDg = new \App\Models\DocumentoDg();
            $docDg->id_documento = $documento->id;
            $docDg->km = $vehiculo->km;
            $docDg->motor = $vehiculo->motor;
            $docDg->color = $vehiculo->color;
            $docDg->caja = $vehiculo->caja;
            $docDg->cilindraje = $vehiculo->cilindraje;
            $docDg->estado_general = Input::get('estado_general');
            $docDg->nivel_combustible = Input::get('nivel_combustible');
            $docDg->avaluo = (float) Input::get("avaluo");

            if (!$docDg->save()) {
                throw new Exception('No se pudo guardar los datos del diagn&oacute;stico');
            }

            $items = Input::get("resp");
            $cantidades = Input::get("cant_item");
            $observaciones = Input::get("observaciones_item");

            foreach ($items as $i => $opcion) {
                $dg = new \App\Models\Diagnostico();
                $dg->id_doc_dg = $docDg->id;
                $dg->id_item_dg = $i;
                $dg->id_opc_dg = $opcion;
                $dg->cantidad = sizeof($cantidades) && array_key_exists($i, $cantidades) ? (int) $cantidades[$i] : 0;
                $dg->observaciones = $observaciones[$i];

                if (!$dg->save()) {
                    throw new Exception('No se pudo guardar un detalle del diagn&oacute;stico');
                }
            }

            $imagenes = Input::file("imagenes");

            if (sizeof($imagenes)) {
                foreach ($imagenes as $imagen) {
                    if (sizeof($imagen) && $imagen->isValid()) {
                        $imgDg = new \App\Models\ImagenDg();
                        $imgDg->id_doc_dg = $docDg->id;
                        $imgDg->archivo = $imagen->getClientOriginalExtension();

                        if (!$imgDg->save()) {
                            throw new Exception('No se pudo guardar una de las ima&aacute;genes');
                        }

                        $path = public_path('storage/imagenes/' . $tipoDoc->sigla . '/' . $documento->id . '/');

                        if (!is_dir($path)) {
                            $partesPath = ['storage', 'imagenes', $tipoDoc->sigla, $documento->id];
                            $subpath = public_path() . '/';
                            foreach ($partesPath as $p) {
                                $subpath .= $p . '/';

                                if (!is_dir($subpath)) {
                                    mkdir($subpath);
                                }
                            }
                        }

                        $file = $imgDg->id . "." . $imgDg->archivo;
                        $imagen->move($path, $file);
                    }
                }
            } else {
                throw new Exception('No hay imagenes');
            }

            $partes = Input::get("dato_mapa");
            if (sizeof($partes)){
                foreach ($partes as $i => $parte) {
                    if ($parte == 1) {
                        $parteDg = new \App\Models\ParteDg();
                        $parteDg->id_doc_dg = $docDg->id;
                        $parteDg->id_parte = $i;

                        if (!$parteDg->save()) {
                            throw new Exception('No se pudo guardar un detalle del mapa del carro');
                        }
                    }
                }
            }

            DB::commit();
        } catch (Exception $e) {
            DB::rollback();
            return $this->retornarError($e);
        }

        return Redirect::action('PeritajeController@mostrarIndex')->with("mensaje", "Documento guardado exitosamente");
    }

    public function buscarVehiculo($id) {
        $vehiculo = \App\Models\Vehiculo::where("id", $id)->with("tipo.partes")->first();
        if (!sizeof($vehiculo)) {
            $vehiculo = new \App\Models\Vehiculo();
        }

        return $vehiculo->toJson();
    }
    
    public function buscarPartes($id) {
        $partes = \App\Models\TipoVehiculo::where("id", $id)->with("partes")->get();        
        if (!sizeof($partes)) {
            $partes = new \App\Models\TipoVehiculo();
        }

        return $partes->toJson();
    }

    public function imprimirPdf($id) {
        $documento = \App\Models\Documento::with(array("docDiagnostico.imagenes", "docDiagnostico.partes.parteVehiculo", "usuarioCreacion", "cliente", "tipoDocumento", "vehiculo.tipo"))->where("id", $id)->first();

        if (!sizeof($documento)) {
            return Redirect::action('PeritajeController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        } else {
            $opciones = \App\Models\OpcionDg::all();

            $categorias = \App\Models\CategoriaDg::with(array(
                "items" => function ($q) use ($documento) {
                    $q->whereHas("tipos", function($q) use ($documento) {
                        $q->where("id_tipo", $documento->id_tipo);
                    });
                },
                "items.tipos" => function($q) use ($documento) {
                    $q->where("id_tipo", $documento->id_tipo);
                },
                "items.diagnosticos" => function($q) use ($documento) {
                    $q->where("id_doc_dg", $documento->docDiagnostico->first()->id);
                }
            ))->whereHas("items.tipos", function($q) use ($documento) {
                $q->where("id_tipo", $documento->id_tipo);
            })->whereHas("items.diagnosticos", function($q) use ($documento) {
                $q->where("id_doc_dg", $documento->docDiagnostico->first()->id);
            })->get();

            $html = View::make("peritaje.impresionPdf", array("documento" => $documento, "categorias" => $categorias, "opciones" => $opciones))->render();
            //echo $html ; die();
            try{
                $pdf = new \App\Lib\myPdf();
                $pdf->render($html, "Documento_" . $documento->tipoDocumento->sigla . "_" . $documento->id);
            } catch (Exception $ex) {
                echo $ex->getMessage();
            }
        }
    }

}
