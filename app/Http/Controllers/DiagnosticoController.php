<?php

namespace App\Http\Controllers;

use View;
use Input;
use Redirect;
use App\Http\Controllers\AdminController;
use App\Models\ItemDg;
use App\Models\CategoriaDg;
use App\Models\TipoDocumento;

class DiagnosticoController extends AdminController {

    public function mostrarIndex(){
        $categorias = CategoriaDg::with("items.tipos")->get();
        $documentos = TipoDocumento::where('tipo_mov', 'O')->orWhere('tipo_mov', 'S')->orWhere('tipo_mov', 'P')->get();
        return View::make('diagnostico.index', array("categorias" => $categorias, "documentos" => $documentos));
    }

    public function mostrarFormCategoria($categoria){
        if (!sizeof($categoria)){
            $categoria = new Categoria();
        }

        return View::make("diagnostico.formCategoria", array("categoria" => $categoria));
    }

    public function crearCategoria(){
        return $this->mostrarFormCategoria(new CategoriaDg());
    }

    public function editarCategoria($id){
        $categoria = CategoriaDg::find($id);
        if (!sizeof($categoria)){
            return Redirect::action('DiagnosticoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar la categoria");
        }

        return $this->mostrarFormCategoria($categoria);
    }

    public function guardarCategoria(){
        $id = Input::get("id");

        $categoria = CategoriaDg::find($id);
        if (!sizeof($categoria)){
            $categoria = new CategoriaDg();
        }

        $categoria->fill(Input::all());

        if ($categoria->save()){
            return Redirect::action('DiagnosticoController@mostrarIndex')->with("mensaje", "Categoria guardada exitosamente");
        }
        else{
            return Redirect::action('DiagnosticoController@mostrarIndex')->with("mensajeError", "No se pudo guardar la categoria");
        }
    }

    public function mostrarFormItem($item){
        if (!sizeof($item)){
            $item = new ItemDg();
        }

        $categorias = CategoriaDg::all();
        return View::make("diagnostico.formItem", array("item" => $item, "categorias" => $categorias));
    }

    public function crearItem(){
        return $this->mostrarFormItem(new ItemDg());
    }

    public function editarItem($id){
        $item = ItemDg::find($id);
        if (!sizeof($item)){
            return Redirect::action('DiagnosticoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el item");
        }

        return $this->mostrarFormItem($item);
    }

    public function guardarItem(){
        $id = Input::get("id");

        $item = ItemDg::find($id);
        if (!sizeof($item)){
            $item = new ItemDg();
        }

        $item->fill(Input::all());

        if ($item->save()){
            return Redirect::action('DiagnosticoController@mostrarIndex')->with("mensaje", "Item guardado exitosamente");
        }
        else{
            return Redirect::action('DiagnosticoController@mostrarIndex')->with("mensajeError", "No se pudo guardar el item");
        }
    }

    public function asociarItemsDoc(){
        $vars = Input::get('doc');

        foreach ($vars as $k=>$v){
            $item = ItemDg::find($k);
            if (sizeof($item)){
                $item->tipos()->sync($v);
            }
        }

        return Redirect::action('DiagnosticoController@mostrarIndex')->with("mensaje", "Asociación terminada");
    }
}