<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MovInvController
 *
 * @author Fabian
 */
 
namespace App\Http\Controllers;

use App\Http\Controllers\AdminController;

class NominaController extends AdminController {

    public function mostrarIndex(){
        $periodos = \App\Models\Periodo::orderBy("fecha_inicio", "desc")->paginate(20);
        return \View::make('nomina.index', array("periodos" => $periodos));
    }
    
    public function obtenerListaPeriodos(){
        $fechaInicio = !empty(\Input::get("fecha_inicio")) ? \Input::get("fecha_inicio") : '2000-01-01';
        $fechaFin = !empty(\Input::get("fecha_fin")) ? \Input::get("fecha_fin") : '2100-12-31';
        $cedula = \Input::get("cedula");
        
        $periodos =  \App\Models\Periodo::where('fecha_inicio', '>=', $fechaInicio)
        ->where('fecha_fin', '<=', $fechaFin)
        ->orderBy("fecha_inicio", "desc");
        
        if (!empty($cedula)){
            $periodos = $periodos->whereHas("resumenes.empleado", function($q) use ($cedula) {
                $q->where("cedula", $cedula);
            });
        }
        
        $periodos = $periodos->orderBy('fecha_inicio', 'desc')->paginate(20);
        
        return \View::make('nomina.lista', array("periodos" => $periodos));
    }
    
    public function mostrarFormPeriodo(){
        $fechaInicio = \App\Models\Periodo::max(\DB::raw("date_add(fecha_fin, INTERVAL 1 DAY)"));
        if (empty($fechaInicio)){
            $fechaInicio = \App\Models\Documento::min("fecha");
        }
        
        return \View::make('nomina.form', array("fechaInicio" => $fechaInicio));
    }
    
    public function liquidarPeriodo(){
        try{
            \DB::beginTransaction();
            
            if (\App\Models\Documento::count() < 1){
                throw new Exception("No hay documentos");
            }
            
            $fechaInicio = \App\Models\Periodo::max(\DB::raw("date_add(fecha_fin, INTERVAL 1 DAY)"));
            
            if (empty($fechaInicio)){
                $fechaInicio = \App\Models\Documento::min("fecha");
            }
            
            if (empty($fechaInicio)){
                throw new Exception("No se pudo obtener una fecha de inicio");
            }
            
            $fechaFin = \Input::get("fecha_fin");
            if (empty($fechaFin)){
                throw new Exception("Debe seleccionar la fecha de fin");
            }
            
            if ($fechaFin > date('Y-m-d')){
                throw new Exception("La fecha de fin no puede ser superior a la fecha actual");
            }
            
            if ($fechaInicio > $fechaFin){
                throw new Exception("La fecha de inicio no puede ser posterior a la fecha de fin");
            }
            
            $documentos = \App\Models\Documento::with(array("tipoDocumento", "factura.servicios", "docDiagnostico.prestamos", "periodo"))
                    ->whereHas("tipoDocumento", function($q) {
                        $q->whereIn("tipo_mov", ["F", "S"]);
                    })
                    ->where("fecha", ">=", $fechaInicio)
                    ->where("fecha", "<=", $fechaFin)
                    ->get();
                    
            if (!sizeof($documentos)){
                throw new Exception("No hay documentos para liquidar");
            }
            
            $periodo = new \App\Models\Periodo();
            //$periodo->codigo = date_format($fechaInicio, 'Ymd')."-".date_format($fechaFin, 'Ymd');
            $periodo->codigo = str_replace("-", "", $fechaInicio)."-".str_replace("-", "", $fechaFin);
            $periodo->fecha_inicio = $fechaInicio;
            $periodo->fecha_fin = $fechaFin;
            $periodo->estado = 'N';
            $periodo->id_usuario_crea = \Auth::user()->id;
            $periodo->fecha_liq = date('Y-m-d');
            
            if (!$periodo->save()){
                throw new Exception("No se pudo crear el periodo de liquidación");
            }
            
            $docsPeriodo = [];
            foreach ($documentos as $d){
                if (sizeof($d->periodo)){
                    continue;
                }
                
                if ($d->tipoDocumento->tipo_mov == "F"){
                    foreach($d->factura as $f){
                        foreach ($f->servicios as $s){
                            $valor = $f->valor_total * $s->porcentaje / 100;
                            $detalle = new \App\Models\PeriodoDetalle();
                            $detalle->id_empleado = $s->id_empleado;
                            $detalle->id_documento = $d->id;
                            $detalle->id_periodo = $periodo->id;
                            $detalle->id_servicio = $s->id_producto;
                            $detalle->id_cargo = $s->id_cargo;
                            $detalle->fecha = $d->fecha;
                            $detalle->valor = $valor;
                            
                            if (!$detalle->save()){
                                throw new Exception("No se pudo guardar los detalles de liquidación (facturas)");
                            }
                        }
                    }
                }
                elseif ($d->tipoDocumento->tipo_mov == "S"){
                    $docDg = $d->docDiagnostico[0];
                    foreach ($docDg->prestamos as $p){
                        $detalle = new \App\Models\PeriodoDetalle();
                        $detalle->id_empleado = $p->id_empleado;
                        $detalle->id_documento = $d->id;
                        $detalle->id_periodo = $periodo->id;
                        $detalle->id_servicio = null;
                        $detalle->id_cargo = null;
                        $detalle->fecha = $d->fecha;
                        $detalle->valor = -$p->valor;

                        if (!$detalle->save()){
                            throw new Exception("No se pudo guardar los detalles de liquidación (préstamos)");
                        }
                    }
                }
                
                $docsPeriodo[] = $d->id;
            }
            
            $periodo->documentos()->attach($docsPeriodo);
            
            \DB::statement('insert into sis_nom_periodo_empleado_resumen (id_empleado, id_periodo, valor) 
                            select id_empleado, id_periodo, sum(valor) from sis_nom_periodo_empleado_detalle
                            where id_periodo = :periodo
                            group by id_empleado, id_periodo', ["periodo" => $periodo->id]);
            
            \DB::commit();
        } catch (Exception $ex) {
            \DB::rollback();
            return $this->retornarError($e);
        }
    }
    
    public function verDetalle($id){
        $periodo = \App\Models\Periodo::where("id", $id)->with("resumenes.empleado")->first();
        
        if (!sizeof($periodo)){
            return Redirect::action('NominaController@mostrarIndex')->with("mensajeError", "Periodo no encontrado");
        }
        
        return \View::make('nomina.detalle', array("periodo" => $periodo));
    }
    
    public function verRecibo($id, $idEmpleado){
        $periodo = \App\Models\Periodo::where("id", $id)->with([
            "resumenes" => function($q) use ($idEmpleado){
                $q->where("id_empleado", $idEmpleado);
            },
            "detalles" => function($q) use ($idEmpleado){
                $q->where("id_empleado", $idEmpleado);
            },
            "detalles.servicio",
            "detalles.cargo",
            "detalles.documento.tipoDocumento"
        ])->first();
        
        if (!sizeof($periodo)){
            return Redirect::action('NominaController@mostrarIndex')->with("mensajeError", "Periodo no encontrado");
        }

        $empleado = \App\User::find($idEmpleado);
        if (!sizeof($empleado)){
            return Redirect::action('NominaController@mostrarIndex')->with("mensajeError", "Empleado no encontrado");
        }
        
        $html = \View::make('nomina.reciboPdf', array("periodo" => $periodo, "empleado" => $empleado))->render();
        $pdf = new \App\Lib\myPdf();
        $pdf->render($html, "Recibo_".str_replace(" ", "-", $empleado->nombre)."_".$periodo->codigo);
    }
}