<?php

namespace App\Http\Controllers;

use View;
use Input;
use Redirect;
use App\Http\Controllers\AdminController;
use App\Models\ParteVehiculo;

class ParteVehiculoController extends AdminController {

    public function mostrarIndex(){
        $partes = ParteVehiculo::paginate(20);
        return View::make('parte_vehiculo.index', array("partes" => $partes));
    }
    
    public function mostrarFormParte($parte){
        if (!sizeof($parte)){
            $parte = new ParteVehiculo();
        }
        
        return View::make("parte_vehiculo.form", array("parte" => $parte));
    }
    
    public function crearParte(){
        return $this->mostrarFormParte(new ParteVehiculo());
    }
    
    public function editarParte($id){
        $parte = ParteVehiculo::find($id);
        if (!sizeof($parte)){
            return Redirect::action('ParteVehiculoController@mostrarIndex')->with("mensajeError", "No se pudo encontrar la parte de vehiculo");
        }
        
        return $this->mostrarFormParte($parte);
    }
    
    public function guardarParte(){
        $id = Input::get("id");

        $parte = ParteVehiculo::find($id);
        if (!sizeof($parte)){
            $parte = new ParteVehiculo();
        }

        $parte->fill(Input::all());
        
        if ($parte->save()){
            return Redirect::action('ParteVehiculoController@mostrarIndex')->with("mensaje", "Parte de vehículo guardada exitosamente");
        }
        else{
            return Redirect::action('ParteVehiculoController@mostrarIndex')->with("mensajeError", "No se pudo guardar la parte de vehículo");
        }
    }
}