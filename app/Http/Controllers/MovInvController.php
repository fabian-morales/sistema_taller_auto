<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MovInvController
 *
 * @author Fabian
 */
 
namespace App\Http\Controllers;

use View;
use Input;
use Auth;
use DB;
use Exception;
use Redirect;
use Response;
use App\Http\Controllers\AdminController;
use App\Models\Documento;
use App\Models\TipoDocumento;
use App\Models\Movimiento;
use App\Models\Producto;
 
class MovInvController extends AdminController {

    public function mostrarIndex(){               
        $movimientos = Documento::with('tipoDocumento')->whereHas('tipoDocumento', function($query) {
            $query->where('tipo_mov', 'M');
        })->orderBy('created_at', 'desc')->paginate(20);
        return View::make('movinv.index', array("movimientos" => $movimientos));
    }
    
    public function mostrarFormMovimiento($documento){
        if (!sizeof($documento)){
            $documento = new Documento();
        }
        
        $tipos = TipoDocumento::where("tipo_mov", "M")->get();
        $productos = Producto::where("tipo", "P")->orWhere("tipo", "H")->get();
        
        return View::make("movinv.form", array("documento" => $documento, "tipos" => $tipos, "productos" => $productos));
    }
    
    public function crearMovimiento(){       
        return $this->mostrarFormMovimiento(new Documento());
    }
    
    /*public function editarUsuario($id){        
        $usuario = User::find($id);
        if (!sizeof($usuario)){
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el usuario");
        }
        
        return $this->mostrarFormUsuario($usuario);
    }*/
    
    public function guardarMovimiento(){        
        try {
            DB::beginTransaction();
            
            $documento = new Documento();
            $idTipoDoc = Input::get("id_tipo");
            $tipoDoc = TipoDocumento::find($idTipoDoc);

            $documento->id_tipo = $idTipoDoc;
            $documento->fecha = date('Y-m-d H:i:s');
            $documento->id_usuario_crea = Auth::user()->id;

            if (!$documento->save()){
                throw new Exception('No se pudo guardar el documento');
            }
            
            $productos = Input::get("id_producto");
            $cantidades = Input::get("cantidad");

            foreach ($productos as $k => $p){
                if ($cantidades[$k] <= 0){
                    throw new Exception('Hay registros sin cantidades');
                }
                
                $producto = Producto::find($p);

                if (!sizeof($producto)){
                    throw new Exception('El producto no extiste');
                }

                $cantidad = $cantidades[$k] * ($tipoDoc->tipo == "S" ? -1 : 1);

                if ($producto->existencias + $cantidad < 0){
                    throw new Exception('El producto '.$producto->id.' - '.$producto->nombre.' no tiene suficientes existencias');
                }

                $movimiento = new Movimiento();
                $movimiento->id_documento = $documento->id;
                $movimiento->id_producto = $p;
                $movimiento->cantidad = $cantidad;
                if (!$movimiento->save()){
                    throw new Exception('No se pudo guardar el movimiento');
                }

                $producto->existencias += $movimiento->cantidad;

                if (!$producto->save()){
                    throw new Exception('No se pudo modificar las existencias del producto '.$p.' - '.$producto->nombre);
                }
            }
            
            DB::commit();
        }
        catch(Exception $e){
            DB::rollback();
            //throw $e;
            return $this->retornarError($e);
        }
        
        return Redirect::action('MovInvController@mostrarIndex')->with("mensaje", "Documento guardado exitosamente");
    }
    
    public function verDetalle($id){
        $documento = Documento::with(array("movimientos.producto", "usuarioCreacion", "tipoDocumento"))->where("id", $id)->first();
        
        if (!sizeof($documento)){
            return Redirect::action('MovInvController@mostrarIndex')->with("mensajeError", "Documento no encontrado");            
        }
        else{
            return View::make("movinv.detalle", array("documento" => $documento));
        }
    }
}