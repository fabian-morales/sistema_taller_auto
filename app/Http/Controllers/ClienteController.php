<?php

namespace App\Http\Controllers;

use View;
use Input;
use Redirect;
use App\Http\Controllers\AdminController;
use App\Models\Cliente;

class ClienteController extends AdminController {

    public function mostrarIndex(){       
        $clientes = Cliente::paginate(20);
        return View::make('cliente.index', array("clientes" => $clientes));
    }
    
    public function obtenerListaClientes(){
        $nombres = Input::get("nombre");
        $apellidos = Input::get("apellidos");
        $cedula = Input::get("cedula");
        
        $clientes = Cliente::orderBy('id');
        
        if (!empty($cedula)){
            $clientes = $clientes->where("id_nit", $cedula);
        }
        
        if (!empty($nombres)){
            $clientes = $clientes->where("nombres", "like", "%".$nombres."%");
        }
        
        if (!empty($apellidos)){
            $clientes = $clientes->where("apellidos", "like", "%".$apellidos."%");
        }
        
        $clientes = $clientes->paginate(20);
        $clientes->setPath('clientes');
        
        return View::make('cliente.lista', array("clientes" => $clientes));
    }
    
    public function mostrarFormCLiente($cliente){
        if (!sizeof($cliente)){
            $cliente = new Cliente();
        }
        
        return View::make("cliente.form", array("cliente" => $cliente));
    }
    
    public function crearCliente(){
        return $this->mostrarFormCliente(new Cliente());
    }
    
    public function editarCliente($id){
        $cliente = Cliente::where("id",$id)->with("vehiculos.tipo")->first();
        if (!sizeof($cliente)){
            return Redirect::action('ClienteController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el cliente");
        }
        
        return $this->mostrarFormCliente($cliente);
    }
    
    public function guardarCliente(){
        $id = Input::get("id");
                
        $cliente = Cliente::find($id);
        if (!sizeof($cliente)){
            $cliente = new Cliente();
        }
                
        $cliente->fill(Input::all());
        
        if ($cliente->save()){
            return Redirect::action('ClienteController@mostrarIndex')->with("mensaje", "Cliente guardado exitosamente");
        }
        else{
            return Redirect::action('ClienteController@mostrarIndex')->with("mensajeError", "No se pudo guardar el cliente");
        }
    }
    
    public function buscarCliente(){
        $cedula = Input::get("cedula");
        $cliente = Cliente::where("id_nit", $cedula)->with("vehiculos")->first();
        if (!sizeof($cliente)){
            $cliente = new Cliente();
        }
        
        return $cliente->toJson();
    }
}