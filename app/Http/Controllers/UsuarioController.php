<?php

namespace App\Http\Controllers;

use View;
use Input;
use Redirect;
use Session;
use Hash;
use App\Http\Controllers\AdminController;
use App\User;
use App\Models\Cargo;
use App\Models\Controlador;

class UsuarioController extends AdminController {

    public function mostrarIndex(){       
        $usuarios = User::with("cargos")->paginate(20);
        return View::make('usuario.index', array("usuarios" => $usuarios));
    }
    
    public function obtenerListaUsuarios(){
        $cedula = Input::get("cedula");
        $nombre = Input::get("nombre");
        
        $usuarios =  User::orderBy("created_at");
        
        if (!empty($cedula)){
            $usuarios = $usuarios->where("cedula", $cedula);
        }
        
        if (!empty($nombre)){
            $usuarios = $usuarios->where("nombre", "like", "%".$nombre."%");
        }
        
        $usuarios = $usuarios->paginate(20);
        
        return View::make('usuario.lista', array("usuarios" => $usuarios));
    }
    
    public function mostrarFormUsuario($usuario){       
        if (!sizeof($usuario)){
            $usuario = new User();
        }
        
        $cargos = Cargo::with(array("empleados" => function($q) use($usuario) {
            $q->where("id_empleado", $usuario->id);
        }))->get();
        
        $controladores = Controlador::with(array('usuarios' => function($query) use($usuario) {
            $query->where('id_usuario', $usuario->id);
        }))->get();
        
        return View::make("usuario.form", array("usuario" => $usuario, "cargos" => $cargos, "controladores" => $controladores));
    }
    
    public function crearUsuario(){       
        return $this->mostrarFormUsuario(new User());
    }
    
    public function editarUsuario($id){        
        $usuario = User::find($id);
        if (!sizeof($usuario)){
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el usuario");
        }
        
        return $this->mostrarFormUsuario($usuario);
    }
    
    public function guardarUsuario(){        
        $id = Input::get("id");
        $clave = Input::get("password");
        
        $usuario = User::find($id);
        if (!sizeof($usuario)){
            $usuario = new User();
        }
        
        if (empty($id) && empty($clave)){
            Session::flash("mensajeError", "Debe ingresar la clave para el usuario");
            return $this->mostrarFormUsuario($usuario);
        }
        else if (!empty($clave)){
            $clave = Hash::make($clave);
        }
        else{
            $clave = $usuario->password;
        }
        
        $usuario->fill(Input::all());
        $usuario->password = $clave;
        
        $cntLogin = User::where("login", $usuario->login)->where("id", "!=", $usuario->id)->count();
        if ($cntLogin > 0){
            Session::flash("mensajeError", "Ya existe un usuario con el login ingresado");
            return $this->mostrarFormUsuario($usuario);
        }
        
        if ($usuario->save()){            
            $cargos = Input::get("id_cargo");
            if (sizeof($cargos)){
                $usuario->cargos()->sync($cargos);
            }
            
            $controladores = Input::get("controlador");
            $usuario->controladores()->attach($controladores);
            
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensaje", "Usuario guardado exitosamente");
        }
        else{
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo guardar el usuario");
        }
    }
    
    public function mostrarFormPermisos($id){
        $usuario = User::find($id);
        if (!sizeof($usuario)){
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el usuario");
        }
        
        $controladores = Controlador::with(array('usuarios' => function($query) use($usuario) {
            $query->where('id_usuario', $usuario->id);
        }))->get();
        
        return View::make('usuario.form_permisos', array("usuario" => $usuario, "controladores" => $controladores));
    }
    
    public function guardarPermisos(){
        $usuario = User::find(Input::get("id_usuario"));
        if (!sizeof($usuario)){
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo encontrar el usuario");
        }
        
        $controladores = Input::get("controlador");
        $usuario->controladores()->sync($controladores);
        return Redirect::action('UsuarioController@mostrarIndex')->with("mensaje", "Permisos asignados exitosamente");
    }
}