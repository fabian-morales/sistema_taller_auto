<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MovInvController
 *
 * @author Fabian
 */
 
namespace App\Http\Controllers;

use stdClass;
use View;
use Input;
use Auth;
use Redirect;
use Response;
use Session;
use DB;
use Exception;
use App\Http\Controllers\AdminController;
use App\User;
use App\Models\Documento;
use App\Models\TipoDocumento;
use App\Models\Factura;
use App\Models\Producto; 
use App\Models\Cliente; 
use App\Models\MedioPago;
use App\Models\ServicioCargo;
use App\Models\ServicioFac;
use App\Models\Cargo;
use App\Models\AbonoFac;
use App\Lib\myPdf;

class FacturacionController extends AdminController {

    public function mostrarIndex(){
        $facturas = Documento::with('tipoDocumento')->whereHas('tipoDocumento', function($query) {
            $query->where('tipo_mov', 'F');
        })->orderBy('created_at', 'desc')->paginate(20);
        
        return View::make('facturacion.index', array("facturas" => $facturas));
    }
    
    public function obtenerListaDocs(){
        $fechaInicio = !empty(Input::get("fecha_inicio")) ? Input::get("fecha_inicio") : '2000-01-01';
        $fechaFin = !empty(Input::get("fecha_fin")) ? Input::get("fecha_fin") : '2100-12-31';
        $cedula = Input::get("cedula");
        $placa = Input::get("placa");
        
        $facturas = Documento::with('tipoDocumento')->whereHas('tipoDocumento', function($query) {
            $query->where('tipo_mov', 'F');
        })->where('fecha', '>=', $fechaInicio)->where('fecha', '<=', $fechaFin);
        
        if (!empty($cedula)){
            $facturas = $facturas->whereHas("cliente", function($q) use ($cedula) {
                $q->where("id_nit", $cedula);
            });
        }
        
        if (!empty($placa)){
            $facturas = $facturas->whereHas("vehiculo", function($q) use ($placa) {
                $q->where("placa", $placa);
            });
        }
        
        $facturas = $facturas->orderBy('created_at', 'desc')->paginate(20);
        
        return View::make('facturacion.lista', array("facturas" => $facturas));
    }
    
    public function mostrarFormFactura($documento){
        if (!sizeof($documento)){
            $documento = new Documento();
        }
        
        $tipos = TipoDocumento::where("tipo_mov", "F")->get();
        $tiposVeh = \App\Models\TipoVehiculo::all();
        $productos = Producto::where("tipo", "P")->orWhere("tipo", "S")->orderBy("nombre", "asc")->get();
        $clientes = Cliente::all();
        $medios = MedioPago::all();
        
        return View::make("facturacion.form", array("documento" => $documento, "tipos" => $tipos, "productos" => $productos, "clientes" => $clientes, "medios" => $medios, "tipos_veh" => $tiposVeh));
    }
    
    public function crearFactura(){       
        return $this->mostrarFormFactura(new Documento());
    }
    
    public function guardarSesion(){
        $json = Response::json(Input::get("json"));
        Session::put('facturacion.documento', $json);
    }
    
    public function obtenerSesion(){
        return Session::get('facturacion.documento');
    }
    
    public function mostrarFormServicios($idServicio){
        $producto = Producto::where("id", $idServicio)->with("encargados.cargo.empleados")->first();
        $servicios = json_decode(Session::get('facturacion.servicios'));
        $_servicios = array();
        $nombrePropiedad = "producto_".$producto->id;
        
        if (sizeof($servicios) && property_exists($servicios, $nombrePropiedad)){
            $_servicios = (array)$servicios->$nombrePropiedad;
        }
        
        return View::make('facturacion.formServicios', array('producto' => $producto, "servicios" => $_servicios));
    }
    
    public function buscarVehiculo($id) {
        $vehiculo = \App\Models\Vehiculo::where("id", $id)->first();
        if (!sizeof($vehiculo)) {
            $vehiculo = new \App\Models\Vehiculo();
        }

        return $vehiculo->toJson();
    }
    
    public function guardarServicio(){
        $idProducto = Input::get("id_producto");
        $cargos = Input::get("id_cargo");
        $empleados = Input::get("id_empleado");
        $servicios = json_decode(Session::get('facturacion.servicios'));
        
        if (!sizeof($servicios)){
            $servicios = new stdClass();
        }
        
        $_cargos = array();
        
        foreach ($cargos as $i => $c){
            $item = new stdClass();
            $item->id_cargo = $c;
            $item->id_empleado = $empleados[$i];
            $item->id_producto = $idProducto;
            $_cargos["cargo_".$c] = $item;
        }
        
        $nombrePropiedad = "producto_".$idProducto;
        $servicios->$nombrePropiedad = $_cargos;
        
        Session::put('facturacion.servicios', json_encode($servicios));
        
        return Session::get('facturacion.servicios');
    }
        
    public function guardarFactura(){
        
        try {
            DB::beginTransaction();
            
            $documento = new Documento();
            $idTipoDoc = Input::get("id_tipo");
            $tipoDoc = TipoDocumento::find($idTipoDoc);
            
            $cedula = Input::get("cedula");
            $nombre = Input::get("nombre");
            $apellidos = Input::get("apellidos");
            $telefono = Input::get("telefono");
            $direccion = Input::get("direccion");

            $cliente = Cliente::where("id_nit", $cedula)->first();
            if (!sizeof($cliente)){
                $cliente = new Cliente();
                $cliente->id_nit = $cedula;
            }

            $cliente->nombres = $nombre;
            $cliente->apellidos = $apellidos;
            $cliente->telefono = $telefono;
            $cliente->direccion = $direccion;

            if (!$cliente->save()){
                throw new Exception('No se pudo guardar los datos del cliente');
            }
            
            $idVehiculo = Input::get("id_vehiculo");
            $vehiculo = \App\Models\Vehiculo::find($idVehiculo);
            
            if (!sizeof($vehiculo) || empty($idVehiculo)){
                //throw new Exception('El veh&iacute;culo ingresado no existe');
                $vehiculo = new \App\Models\Vehiculo();
                $vehiculo->id_propietario = $cliente->id;
            }
            
            $vehiculo->id_tipo = Input::get("tipo_vehiculo");    
            if (empty($vehiculo->id_tipo)){
                throw new Exception('Debe seleccionar el tipo de vehículo');
            }
            
            $vehiculo->marca = Input::get("marca");
            if (empty($vehiculo->marca)){
                throw new Exception('Debe seleccionar la marca del vehículo');
            }
            
            $vehiculo->motor = Input::get("motor");
            $vehiculo->serie = Input::get("serie");
            $vehiculo->color = Input::get("color");
            $vehiculo->modelo = Input::get("modelo");
            $vehiculo->placa = Input::get("placa");
            if (empty($vehiculo->placa)){
                throw new Exception('Debe seleccionar la placa del vehículo');
            }
            
            $vehiculo->km = Input::get("km");
            $vehiculo->cilindraje = Input::get("cilindraje");
            $vehiculo->caja = Input::get("caja");
            $vehiculo->nombre = $vehiculo->marca." - ".$vehiculo->placa;
            
            if (!$vehiculo->save()){
                throw new Exception('No se pudo crear el vehículo');
            }
            
            if ($vehiculo->id_propietario !== $cliente->id){
                throw new Exception('El vehículo ingresado no pertenece al cliente');
            }
            
            $idRel = Input::get("id_doc_rel");
            
            if (!empty($idRel)){
                $docRel = Documento::where("id", $idRel)->with("docGenerado")->first();
                if (!sizeof($docRel)){
                    throw new Exception('Documento relacionado no existe');
                }
                
                if (sizeof($docRel->docGenerado)){
                    throw new Exception('El documento relacionado ya fue usado');
                }
            }
            else{
                $idRel = null;
            }

            $documento->id_tipo = $idTipoDoc;
            $documento->fecha = date('Y-m-d H:i:s');
            $documento->id_cliente = $cliente->id;
            $documento->observaciones = Input::get("observaciones");
            $documento->id_usuario_crea = Auth::user()->id;
            $documento->id_vehiculo = $vehiculo->id;
            $documento->id_doc_rel = $idRel;

            if (!$documento->save()){
                throw new Exception('No se pudo guardar el documento');
            }
            
            $productos = Input::get("id_producto");
            $cantidades = Input::get("cantidad");
            $descuentos = Input::get("descuento");
            $valoresDcto = Input::get("valor_descuento");
            $valoresUnit = Input::get("valor_unitario");
            $valoresTotal = Input::get("valor_total");
            $mediosPago = Input::get("id_medio");
            $abonos = Input::get("monto_pago");

            $valorDctoDoc = 0;
            $valorBrutoDoc = 0;
            $valorTotalDoc = 0;
            $cantidadDoc = 0;
            $totalAbonosDoc = 0;

            $servicios = array();
            if (Session::has('facturacion.servicios')){
                $servicios = (array)json_decode(Session::get('facturacion.servicios'));    
            }

            foreach ($productos as $k => $p){
                $cantidad = $cantidades[$k];
                if ($cantidad > 0){
                    $producto = Producto::find($p);

                    if (!sizeof($producto)){
                        throw new Exception('El producto '.$p.' no existe');
                    }

                    if ($producto->tipo != "P" && $producto->tipo != "S"){
                        throw new Exception('El tipo del producto '.$p.' - '.$producto->nombre.' no es válido');
                    }

                    if ((int)$valoresUnit[$k] <= 0){
                        throw new Exception('El producto '.$p.' - '.$producto->nombre.' no tiene precio');
                    }

                    $movimiento = new Factura();
                    $movimiento->id_documento = $documento->id;
                    $movimiento->id_producto = $p;
                    $movimiento->cantidad = $cantidad;
                    $movimiento->valor_unitario = $valoresUnit[$k];
                    $movimiento->descuento = $descuentos[$k];
                    $movimiento->valor_descuento = $valoresDcto[$k];
                    $movimiento->iva = 0;
                    $movimiento->valor_iva = 0;
                    $movimiento->valor_total = $valoresTotal[$k];
                    $movimiento->valor_bruto = $movimiento->valor_total + $movimiento->valor_descuento;

                    $valorDctoDoc += $movimiento->valor_descuento;
                    $valorBrutoDoc += $movimiento->valor_bruto;
                    $valorTotalDoc += $movimiento->valor_total;
                    $cantidadDoc += $movimiento->cantidad;

                    if (!$movimiento->save()){
                        throw new Exception('No se pudo guardar el registro del item '.$p.' - '.$producto->nombre.' en el documento');
                    }

                    if ($producto->tipo == "P" && $producto->existencias - $cantidad >= 0){
                        $producto->existencias -= $movimiento->cantidad;
                        if (!$producto->save()){
                            throw new Exception('No se pudo descontar las existencias del producto '.$p.' - '.$producto->nombre);
                        }
                    }
                    elseif($producto->tipo != "S") {
                        throw new Exception('El producto '.$p.' - '.$producto->nombre.' no tiene suficientes existencias');
                    }

                    if ($producto->tipo == "S"){
                        if (!sizeof($servicios)){
                            throw new Exception('No se ha especificado los empleados asignados a los servicios');
                        }

                        $key = "producto_".$p;
                        if (!sizeof($servicios[$key])){
                            throw new Exception('El servicio '.$p.' - '.$producto->nombre.' no tiene asignados los empleados');
                        }

                        $cargos = (array)$servicios[$key];
                        foreach ($cargos as $c){
                            $cargo = Cargo::find($c->id_cargo);
                            if (!sizeof($cargo)){
                                throw new Exception('El cargo '.$c->id_cargo.' no existe');
                            }

                            $empleado = User::find($c->id_empleado);
                            if (!sizeof($empleado)){
                                throw new Exception('El empleado '.$c->id_empleado.' no existe');
                            }

                            $servCargo = ServicioCargo::where("id_producto", $p)->where("id_cargo", $c->id_cargo)->first();
                            if (!sizeof($servCargo)){
                                throw new Exception('El cargo '.$cargo->nombre.' no está asociado al servicio '.$producto->id.' - '.$producto->nombre);
                            }

                            $cntCargoEmpl = $empleado->cargos()->where('id_cargo', $c->id_cargo)->count();
                            if ($cntCargoEmpl <= 0){
                                throw new Exception('El cargo '.$cargo->nombre.' no está asignado al empleado '.$empleado->id.' - '.$empleado->nombre);
                            }

                            $servFac = new ServicioFac();
                            $servFac->id_docfac = $movimiento->id;
                            $servFac->id_documento = $documento->id;
                            $servFac->id_cargo = $c->id_cargo;
                            $servFac->id_empleado = $c->id_empleado;
                            $servFac->id_producto = $c->id_producto;
                            $servFac->porcentaje = $servCargo->porcentaje;

                            if (!$servFac->save()){
                                throw new Exception('No se pudo guardar el registro del cargo '.$cargo->nombre.', empleado '.$empleado->id.' - '.$empleado->nombre.', servicio '.$producto->id.' - '.$producto->nombre);
                            }
                        }
                    }
                }
            }

            foreach ($mediosPago as $k => $m){
                $monto = $abonos[$k];

                if ($monto > 0){
                    $medio = MedioPago::find($m);
                    if (!sizeof($medio)){
                        throw new Exception('El medio de pago '.$m.' no existe');
                    }

                    $abono = new AbonoFac();
                    $abono->id_documento = $documento->id;
                    $abono->id_medio = $m;
                    $abono->valor = $monto;
                    $totalAbonosDoc += $monto;

                    if (!$abono->save()){
                        throw new Exception('No se pudo guardar el registro del abono con '.$medio->nombre.' y valor '.$monto);
                    }
                }
            }

            if ($totalAbonosDoc != $valorTotalDoc){
                throw new Exception('El valor de los abono no coincide con el valor del documento');
            }

            $documento->cantidad = $cantidadDoc;
            $documento->valor_descuento = $valorDctoDoc;
            $documento->valor_iva = 0;
            $documento->valor_bruto = $valorBrutoDoc;
            $documento->valor_total = $valorTotalDoc;

            if (!$documento->save()){
                throw new Exception('No se pudo actualizar los totales del documento');
            }
            
            DB::commit();
            Session::forget('facturacion.servicios');
            Session::forget('facturacion.documento');
        }
        catch(Exception $e){
            DB::rollback();
            return $this->retornarError($e);
            //throw $e;
        }
        
        return Redirect::action('FacturacionController@mostrarIndex')->with("mensaje", "Documento guardado exitosamente");
                
        /*if ($usuario->save()){
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensaje", "Usuario guardado exitosamente");
        }
        else{
            return Redirect::action('UsuarioController@mostrarIndex')->with("mensajeError", "No se pudo guardar el usuario");
        }*/
    }
    
    public function verDetalle($id){
        $documento = Documento::with(array("factura.producto", "usuarioCreacion", "cliente", "tipoDocumento", "abonos.medioPago", "factura.servicios.servicio", "factura.servicios.cargo", "factura.servicios.empleado", "vehiculo.tipo", "docGenerado.tipoDocumento"))->where("id", $id)->first();
        
        if (!sizeof($documento)){
            return Redirect::action('FacturacionController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        }
        else{
            return View::make("facturacion.detalle", array("documento" => $documento));
        }
    }
    
    public function imprimirPdf($id){
        $documento = Documento::with(array("factura.producto", "usuarioCreacion", "cliente", "tipoDocumento", "abonos.medioPago", "factura.servicios.servicio", "factura.servicios.cargo", "factura.servicios.empleado", "vehiculo.tipo", "docGenerado.tipoDocumento"))->where("id", $id)->first();
        
        if (!sizeof($documento)){
            return Redirect::action('FacturacionController@mostrarIndex')->with("mensajeError", "Documento no encontrado");
        }
        else{
            \Carbon\Carbon::setLocale( \Lang::getLocale() );
                       
            $empleados = [];
            
            foreach($documento->factura as $f){
                if ($f->producto->tipo == "S" && sizeof($f->servicios)){
                    foreach ($f->servicios as $s){
                        if (!key_exists($s->empleado->id, $empleados)){
                            $empleados[$s->empleado->id] = ["cedula" => $s->empleado->cedula, "nombre" => $s->empleado->nombre];
                        }
                    }
                }
            }
            
            $html = View::make("facturacion.impresionPdf", array("documento" => $documento, "empleados" => $empleados))->render();
            $pdf = new myPdf();
            $pdf->render($html, "Documento_".$documento->tipoDocumento->sigla."_".$documento->id);
            
            /*$pdf = new myPdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            $pdf->SetCreator(PDF_CREATOR);
            $pdf->SetAuthor('FrontierSoft');
            $pdf->SetTitle('Documento');
            $pdf->SetSubject('Documento');
            $pdf->SetKeywords('Documento');

            $pdf->setPrintHeader(true);
            $pdf->setPrintFooter(true);

            $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
            $pdf->SetFont('helvetica', '', 11);

            $pdf->SetMargins(PDF_MARGIN_LEFT, 45, PDF_MARGIN_RIGHT);
            $pdf->SetAutoPageBreak(TRUE, 16);
            $pdf->setImageScale(1.6);
            $pdf->setJPEGQuality(100);

            $pdf->AddPage('P', 'Letter');

            $pdf->writeHTML($html, true, false, true, false, '');
            $pdf->Output();*/
        }
    }
    
    public function obtenerDatosProducto($id){
        $producto = Producto::find($id);
        if (!sizeof($producto)){
            return json_encode([]);
        }
        else{
            return $producto->toJson();
        }
    }
}