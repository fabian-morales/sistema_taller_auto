<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

Route::get('/', 'HomeController@mostrarInicio');
Route::get('/producto/{id}', 'FacturacionController@obtenerDatosProducto');

Route::group(array('prefix' => 'sesion'), function() {
    Route::get('/formLogin', 'SesionController@mostrarIndex');
    Route::post('/login', 'SesionController@hacerLogin');
});

Route::group(array('prefix' => 'usuarios'), function() {
    Route::get('/', 'UsuarioController@mostrarIndex');
    Route::post('/lista', 'UsuarioController@obtenerListaUsuarios');
    Route::get('/crear', 'UsuarioController@crearUsuario');
    Route::get('/editar/{id}', 'UsuarioController@editarUsuario');
    Route::get('/permisos/{id}', 'UsuarioController@mostrarFormPermisos');
    Route::post('/guardar', 'UsuarioController@guardarUsuario');
    Route::get('/guardarPermisos', 'UsuarioController@guardarPermisos');
});

Route::group(array('prefix' => 'productos'), function() {
    Route::get('/', 'ProductoController@mostrarIndex');
    Route::post('/lista', 'ProductoController@obtenerListaProductos');
    Route::get('/crear', 'ProductoController@crearProducto');
    Route::get('/editar/{id}', 'ProductoController@editarProducto');
    Route::post('/guardar', 'ProductoController@guardarProducto');
    Route::get('/borrar/{id}', 'ProductoController@borrarProducto');
    Route::get('/existencias', 'ProductoController@verExistencias');
    Route::get('/adicionarCargo/{idProducto}', 'ProductoController@crearEncargado');
    Route::get('/editarCargo/{idProducto}', 'ProductoController@editarEncargado');
    Route::post('/guardarCargo', 'ProductoController@guardarEncargado');
    Route::get('/quitarCargo/{id}', 'ProductoController@borrarEncargado');
});

Route::group(array('prefix' => 'movimientos'), function() {
    Route::get('/', 'MovInvController@mostrarIndex');
    Route::get('/crear', 'MovInvController@crearMovimiento');
    Route::post('/guardar', 'MovInvController@guardarMovimiento');
    Route::get('/detalle/{id}', 'MovInvController@verDetalle');
});

Route::group(array('prefix' => 'facturas'), function() {
    Route::get('/', 'FacturacionController@mostrarIndex');
    Route::post('/lista', 'FacturacionController@obtenerListaDocs');
    Route::get('/crear', 'FacturacionController@crearFactura');
    Route::post('/guardar', 'FacturacionController@guardarFactura');
    Route::get('/detalle/{id}', 'FacturacionController@verDetalle');
    Route::get('/pdf/{id}', 'FacturacionController@imprimirPdf');
    Route::post('/sesion', 'FacturacionController@guardarSesion');
    Route::any('/recuperar', 'FacturacionController@obtenerSesion');
    Route::get('/servicios/{idServicio}', 'FacturacionController@mostrarFormServicios');
    Route::post('/servicio/guardar', 'FacturacionController@guardarServicio');
    Route::post('/buscarVehiculo/{id}', 'FacturacionController@buscarVehiculo');
});

Route::group(array('prefix' => 'cargos'), function() {
    Route::get('/', 'CargoController@mostrarIndex');
    Route::get('/crear', 'CargoController@crearCargo');
    Route::get('/editar/{id}', 'CargoController@editarCargo');
    Route::post('/guardar', 'CargoController@guardarCargo');
});

Route::group(array('prefix' => 'clientes'), function() {
    Route::get('/', 'ClienteController@mostrarIndex');
    Route::post('/lista', 'ClienteController@obtenerListaClientes');
    Route::get('/crear', 'ClienteController@crearCliente');
    Route::get('/editar/{id}', 'ClienteController@editarCliente');
    Route::post('/guardar', 'ClienteController@guardarCliente');
    Route::post('/buscar', 'ClienteController@buscarCliente');
});

Route::group(array('prefix' => 'mediospago'), function() {
    Route::get('/', 'MedioPagoController@mostrarIndex');
    Route::get('/crear', 'MedioPagoController@crearMedio');
    Route::get('/editar/{id}', 'MedioPagoController@editarMedio');
    Route::post('/guardar', 'MedioPagoController@guardarMedio');
});

Route::group(array('prefix' => 'diagnostico'), function() {
    Route::get('/', 'DiagnosticoController@mostrarIndex');

    Route::group(array('prefix' => 'categoria'), function() {
        Route::get('/', 'DiagnosticoController@mostrarIndex');
        Route::get('/crear/', 'DiagnosticoController@crearCategoria');
        Route::get('/editar/{id}', 'DiagnosticoController@editarCategoria');
        Route::post('/guardar', 'DiagnosticoController@guardarCategoria');
    });

    Route::group(array('prefix' => 'item'), function() {
        Route::get('/', 'DiagnosticoController@mostrarIndex');
        Route::get('/crear/', 'DiagnosticoController@crearItem');
        Route::get('/editar/{id}', 'DiagnosticoController@editarItem');
        Route::post('/guardar', 'DiagnosticoController@guardarItem');
        Route::post('/asociar/', 'DiagnosticoController@asociarItemsDoc');
    });
});

Route::group(array('prefix' => 'orden'), function() {
    Route::group(array('prefix' => 'entrada'), function() {
        Route::get('/', 'OrdenEntradaController@mostrarIndex');
        Route::post('/lista', 'OrdenEntradaController@obtenerListaDocs');
        Route::get('/detalle/{id}', 'OrdenEntradaController@verDetalle');
        Route::get('/crear/', 'OrdenEntradaController@crearOrden');
        Route::get('/editar/{id}', 'OrdenEntradaController@editarOrden');
        Route::post('/guardar', 'OrdenEntradaController@guardarOrden');
        Route::post('/buscarVehiculo/{id}', 'OrdenEntradaController@buscarVehiculo');
        Route::get('/pdf/{id}', 'OrdenEntradaController@imprimirPdf');
    });

    Route::group(array('prefix' => 'peritaje'), function() {
        Route::get('/', 'PeritajeController@mostrarIndex');
        Route::post('/lista', 'PeritajeController@obtenerListaDocs');
        Route::get('/detalle/{id}', 'PeritajeController@verDetalle');
        Route::get('/crear/', 'PeritajeController@crearPeritaje');
        Route::get('/editar/{id}', 'PeritajeController@editarPeritaje');
        Route::post('/guardar', 'PeritajeController@guardarPeritaje');
        Route::post('/buscarVehiculo/{id}', 'PeritajeController@buscarVehiculo');
        Route::post('/buscarPartes/{id}', 'PeritajeController@buscarPartes');
        Route::get('/pdf/{id}', 'PeritajeController@imprimirPdf');
    });
    
    Route::group(array('prefix' => 'servicio'), function() {
        Route::get('/', 'OrdenServicioController@mostrarIndex');
        Route::post('/lista', 'OrdenServicioController@obtenerListaDocs');
        Route::get('/detalle/{id}', 'OrdenServicioController@verDetalle');
        Route::get('/crear/', 'OrdenServicioController@crearOrden');
        Route::get('/editar/{id}', 'OrdenServicioController@editarOrden');
        Route::post('/guardar', 'OrdenServicioController@guardarOrden');
        Route::post('/buscarVehiculo/{id}', 'OrdenServicioController@buscarVehiculo');
        Route::get('/pdf/{id}', 'OrdenServicioController@imprimirPdf');
        Route::get('/facturar/{id}', 'OrdenServicioController@generarFactura');
        Route::get('/prestamo/{id}', 'OrdenServicioController@mostrarFormPrestamo');
        Route::post('/prestamo/guardar', 'OrdenServicioController@guardarPrestamo');
        Route::get('/prestamo/lista/{id}', 'OrdenServicioController@verListaPrestamos');
    });
});

Route::group(array('prefix' => 'vehiculos'), function() {
    Route::get('/', 'VehiculoController@mostrarIndex');
    Route::post('/lista', 'VehiculoController@mostrarListaVehiculos');
    Route::get('/crear', 'VehiculoController@crearVehiculo');
    Route::get('/editar/{id}', 'VehiculoController@editarVehiculo');
    Route::post('/guardar', 'VehiculoController@guardarVehiculo');

    Route::group(array('prefix' => 'partes'), function() {
        Route::get('/', 'ParteVehiculoController@mostrarIndex');
        Route::get('/crear/', 'ParteVehiculoController@crearParte');
        Route::get('/editar/{id}', 'ParteVehiculoController@editarParte');
        Route::post('/guardar', 'ParteVehiculoController@guardarParte');
    });

    Route::group(array('prefix' => 'tipos'), function() {
        Route::get('/', 'TipoVehiculoController@mostrarIndex');
        Route::get('/crear/', 'TipoVehiculoController@crearTipo');
        Route::get('/editar/{id}', 'TipoVehiculoController@editarTipo');
        Route::post('/guardar', 'TipoVehiculoController@guardarTipo');
    });
});

Route::group(array('prefix' => 'cotizacion'), function() {
    Route::get('/', 'CotizacionController@mostrarIndex');
    Route::post('/lista', 'CotizacionController@obtenerListaDocs');
    Route::get('/crear', 'CotizacionController@crearCotizacion');
    Route::post('/guardar', 'CotizacionController@guardarCotizacion');
    Route::get('/detalle/{id}', 'CotizacionController@verDetalle');
    Route::get('/pdf/{id}', 'CotizacionController@imprimirPdf');
    Route::post('/sesion', 'CotizacionController@guardarSesion');
    Route::any('/recuperar', 'CotizacionController@obtenerSesion');
    Route::get('/facturar/{id}', 'CotizacionController@generarFactura');
    Route::get('/enviar/{id}', 'CotizacionController@enviarEmail');
});

Route::group(array('prefix' => 'nomina'), function() {
    Route::group(array('prefix' => 'periodo'), function() {
        Route::get('/', 'NominaController@mostrarIndex');
        Route::post('/lista', 'NominaController@obtenerListaPeriodos');
        Route::get('/crear', 'NominaController@mostrarFormPeriodo');
        Route::post('/liquidar', 'NominaController@liquidarPeriodo');
        Route::get('/detalle/{id}', 'NominaController@verDetalle');
    });
    
    Route::get('/recibo/{id}/{idEmpleado}', 'NominaController@verRecibo');
});

/* Route::get('/productos/', 'ProductoController@mostrarIndex');
  Route::get('/productos/crear', 'ProductoController@crearProducto');
  Route::get('/productos/editar/{id}', 'ProductoController@editarProducto'); */


Route::get('/crearAdmin', function() {
    $usuario = new App\User();
    $usuario->nombre = "Admin";
    $usuario->login = "admin";
    $usuario->password = \Hash::make("Ev4nerv");
    $usuario->admin = "Y";
    $usuario->save();
});

Route::get('/sesion', function() {
    echo \Session::get('facturacion.documento');
    print_r((array) json_decode(Session::get('facturacion.servicios')));
});
