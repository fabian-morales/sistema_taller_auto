<?php

namespace App\Lib;

use DOMPDF;

define('DOMPDF_ENABLE_REMOTE', true);
require_once("dompdf/dompdf_config.inc.php");

class myPdf{
    public function render($html, $nombre){
        set_time_limit (300);
        $old = ini_set("memory_limit", "1024M");
        
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $dompdf->stream($nombre, array('compress'=>1, 'Attachment' => 0));
        
        ini_set("memory_limit", $old);
    }
    
    public function renderToFile($html, $nombre){
        set_time_limit (300);
        $old = ini_set("memory_limit", "1024M");
        
        $dompdf = new DOMPDF();
        $dompdf->load_html($html);
        $dompdf->render();
        $output = $dompdf->output();
        \Storage::put($nombre, $output);
        
        ini_set("memory_limit", $old);
    }
}